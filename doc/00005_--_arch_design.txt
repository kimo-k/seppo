
Q: how store login credentials for editing etc.?

A: username:bcrypt inside app/etc/auth.hash 

https://en.wikipedia.org/wiki/Bcrypt
https://opam.ocaml.org/packages/safepass/

e.g.

$ cat app/etc/auth.hash
sepp:$2a$10$4WlEAb81axSiQ7rEo0xfPOtNREO8pZNw0lf5DK5OMViBNvsrorQVa

