#!/bin/sh
cd "$(dirname "$0")/../" || exit 1

echo
echo Todo
grep -F Logr.err */*.ml \
| grep -ovE "E\.[e]+[0-9]{4}"
# | cut -d ':' -f1

echo Duplicates?
grep -F Logr.err */*.ml \
| grep -oE "E\.[e]+[0-9]{4}" \
| sort \
| uniq -c \
| sort
