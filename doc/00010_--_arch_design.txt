
Q: o/p/id.0.cdb or o/p-0/id.cdb

A: o/p/id.0.cdb

Reason: keep everything in o/p-0/ immutable, o/p/ has to contain mutable state anyway.
