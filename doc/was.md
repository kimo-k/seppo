
# Was ist Seppo.App?

"Seppo" steht für "self-post" und meint das eigenständige Veröffentlichen (im
Internet) für Laien. Zensurfreie, emanzipierte, aktive Teilhabe am Internet.

Dabei hilft die Seppo App, um auf der eigenen Domain (mit Seppo Server) oder
wahlweise bei pinboard.in kurze Texte (und später Bilder) zu publizieren.

Zu den Monopol-Plattformen kann der Seppo Server weiterleiten. D.h. die Posts
erscheinen dort automatisch auch und werden gefunden. Da jedes Posting auf die
Quelle beim Seppo Server verweist, haben diese Plattformen aber nicht die
Erstveröffentlichung (was auch rechtlich wichtig ist) und auch keine Exklusivität,
d.h. wenn eine Plattform keine Lust mehr auf neue oder alte Postings hat, wissen
alle Stammleserinnen, wo die eigentliche Quelle ist und man kann weiterhin posten.
(indieweb.org/POSSE)

Auch bleiben alle Posts solange man will, in der Form, die man will, sichtbar.


# Wie soll das gehen, das ist doch komplizierte Technik?

Ein Vergleich mit dem Fahrradfahren: Eins zu bauen ist extrem viel Aufwand und
können nur wenige, aber eins benutzen ist für Laien machbar. Genauso ist es sehr
aufwändig einen Server sicher zu betreiben (oder sogar zu bauen). Und je
komplizierter das Fahrrad ist, desto schwieriger ist Bedienung und Wartung.

Ein Ausweg liegt in der Reduktion, dem Weglassen nicht unbedingt nötiger Dinge.


# Was darf ich mit Seppo.app sagen?

Alles.

Emanzipiert ist, wem das Rad gehört und es in Grundzügen warten kann (Luft
aufpumpen).

Kurze Texte im Netz veröffentlichen ohne AGBs und Zensur zu unterliegen kann so
einfach sein wie Radfahren.

Der Vergleich paßt auch bei Miet-Rädern: Es gibt verlockende Räder, die billig oder
scheinbar umsonst benutzbar sind. Damit darf ich aber z.B. einen Bereich nicht
verlassen oder bestimmte Dinge nicht tun und garantiert nicht für immer. Man ist in
dauerhafter Abhängigkeit. Je billiger, desto weniger Rechte.

Wer keinen eigenen Seppo Server mag, kann für einen Dollar pro Monat pinboard.in
benutzen - und unterliegt dann natürlich dessen Bedingungen.


# Was ist im Internet das eigene Rad?

1. eigenständiger Betrieb ohne fremde Hilfe (abgesehen vom Webspace/Hosting),
2. Eigentum des Erscheinungsorts (eigener Domain Name),
3. freie Meinungsäußerung (Grundgesetz, Artikel 5), d.h. es gibt keine
   Einschränkungen durch AGBs oder sonstige Nutzungsbedingungen, Verhaltenskodizes
   Dritter o.ä. Es gelten die lokalen Gesetze von Serverstandort und Wohnort,
4. wenige Funktionen, ausreichend für Microblogging, d.h. Links mit Kurztexten und
   Schlagworten, später Bildern,
5. keine Wartung (keine(!) notwendigen Sicherheitsupdates), einfache
   Abschaltung durch Dateilöschung.


# Wieso Fahrrad? Ich finde SUVs super!

Planeten anderer zerstören ist scheiße. Kümmere Dich zuerst darum. Seppo ist noch
nichts für Dich.


# Was muß ich tun?

## Seppo.App

für iOS aus dem Apple App Store holen.

## Seppo Server

1. für EUR 2,- pro Monat Webspace mit Domain kaufen,
2. das https://seppo.social/Linux-x86_64/seppo.cgi holen,
3. auf den Webspace kopieren,
4. die Dateirechte (chmod) auf Nurlese+Ausführen für alle (numerisch 555) setzen,
5. http://<meine.domain/WoIchsHin/KopiertHabe>/seppo.cgi besuchen.

fertig!

