
document.documentElement.lang = navigator.language;

// inspired by https://stackoverflow.com/a/577002/349514
const link = document.createElement("link");
link.href  = "l10n/" + document.documentElement.lang + "/index.css";
link.type  = "text/css";
link.rel   = "stylesheet";
link.media = "screen,print";
document.getElementsByTagName( "head" )[0].appendChild( link );
