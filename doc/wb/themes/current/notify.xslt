<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/xslt-10/
  https://www.w3.org/TR/xpath-10/
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="atom rdf xsl"
  version="1.0">

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="xml_base">../..</xsl:variable>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'/o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'/themes/current')"/>
  <xsl:variable name="cgi_base" select="concat($xml_base,'/seppo.cgi')"/>

  <xsl:template match="/a:feed">
	<html xmlns="http://www.w3.org/1999/xhtml" data-xml-base-pub="{$xml_base_pub}" class="logged-out script-inactive">
	    <head>
        <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <script src="{$skin_base}/notify.js"></script>
<!-- script src="{$xml_base}/live.js"></script -->
        <title><xsl:value-of select="a:title"/></title>
	    </head>
		<body>
		  <div id="header">
            <a class="btn" href="{$xml_base}" rel="first">🏡 <span class="hidden-xs"></span></a>
			<h1><xsl:value-of select="a:title"/></h1>
		    <xsl:call-template name="prev-next"/>
 		  </div>
			<form id="notify" method="post" action="{$cgi_base}/notify">
				<input type="submit" class="noscript" value="Send"/>
				<ul>
					<xsl:for-each select="a:link[@rel='notify' or @rel='subscribed' ]">
						<li class="actor">
							<input type="checkbox" name="{@title}" checked="checked" disabled="disabled"/>
							<img alt="Avatar" src="{@icon}"/>
			  				  <xsl:value-of select="@title"/>
							  <xsl:text> </xsl:text>
							  <xsl:choose>
						        <xsl:when test='@rfc7033'>
									<a href="{$cgi_base}/{@rfc7033}">
										<xsl:value-of select="substring-after(@rfc7033,'acct:')"/>
									</a>
						        </xsl:when>
						        <xsl:otherwise>
									<a href="{$cgi_base}/actor?id={@href}">
										<xsl:value-of select="@href"/>
									</a>
						        </xsl:otherwise>
					          </xsl:choose>
						</li>
					</xsl:for-each>
				</ul>
				<input type="submit" class="noscript" value="Send"/>
			</form>
		    <xsl:call-template name="prev-next"/>
		</body>
    </html>
  </xsl:template>

  <xsl:template name="prev-next">
    <xsl:if test="a:link[@rel='first'] or a:link[@rel='last']">
      <table class="prev-next">
        <tbody>
          <tr>
            <td class="text-left" aria-label="First">
              <xsl:variable name="disabled"><xsl:if test="a:link[@rel='first']/@href = a:link[@rel='self']/@href">disabled</xsl:if></xsl:variable>
              <a href="{a:link[@rel='first']/@href}" class="first {$disabled} btn"><xsl:value-of select="a:link[@rel='first']/@title"/></a>
            </td>
            <td class="text-center">
              <xsl:variable name="disabled"><xsl:if test="not(a:link[@rel='previous'])">disabled</xsl:if></xsl:variable>
              <a href="{a:link[@rel='previous']/@href}" class="previous {$disabled} btn">&#160;&lt;&#160;</a>
            </td>
            <td class="text-center">
              <span class="hidden-xs"></span><xsl:value-of select="a:link[@rel='self']/@title"/>
            </td>
            <td class="text-center">
              <xsl:variable name="disabled"><xsl:if test="not(a:link[@rel='next'])">disabled</xsl:if></xsl:variable>
              <a href="{a:link[@rel='next']/@href}" class="next {$disabled} btn">&#160;&gt;&#160;</a>
            </td>
            <td class="text-right">
              <xsl:variable name="disabled"><xsl:if test="a:link[@rel='last']/@href = a:link[@rel='self']/@href">disabled</xsl:if></xsl:variable>
              <a href="{a:link[@rel='last']/@href}" class="last {$disabled} btn"><xsl:value-of select="a:link[@rel='last']/@title"/></a>
            </td>
          </tr>
        </tbody>
      </table>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
