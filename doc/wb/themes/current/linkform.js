
//  https://stackoverflow.com/a/23395136/349514
function tick() {
  return new Audio("../themes/current/tick.mp3");
}
document.querySelectorAll('#snippets button').forEach(e => {
  // https://stackoverflow.com/a/30810322/349514
  e.addEventListener('click',evt => {
    evt.preventDefault();
    navigator.clipboard.writeText(e.innerText).then(function() {
      console.log('Async: Copying to clipboard was successful!');
      tick().play();
    }, function(err) {
      console.error('Async: Could not copy text: ', err);
    });
  });
}); 
