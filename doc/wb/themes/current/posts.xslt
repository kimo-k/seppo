<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://w3.org/TR/xslt-10
  https://w3.org/TR/xpath-10
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:a="http://www.w3.org/2005/Atom"
  xmlns:as="https://www.w3.org/ns/activitystreams#"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:ldp="http://www.w3.org/ns/ldp#"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:schema="http://schema.org#"
  xmlns:se="http://seppo.social/"
  xmlns:seppo="http://seppo.social/2023/ns#"
  xmlns:toot="http://joinmastodon.org/ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="a as opensearch ldp media georss rdf se"
  xmlns:math="http://exslt.org/math"
  extension-element-prefixes="math"
  version="1.0">

  <!-- xsl:variable name="redirector">https://anonym.to/?</xsl:variable --> <!-- mask the HTTP_REFERER -->
  <xsl:variable name="redirector"></xsl:variable>
  <xsl:variable name="archive">https://web.archive.org/web/</xsl:variable>

  <xsl:include href="util.xslt"/>

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <!-- http://stackoverflow.com/a/16328207 -->
  <xsl:key name="CategorY" match="a:entry/a:category" use="@term" />

  <xsl:variable name="self" select="/*/a:link[@rel = 'self']/@href"/>
  <xsl:variable name="xml_base_absolute" select="/*/@xml:base"/>
  <!-- a bit hairy, but actually works -->
  <xsl:variable name="xml_base_relative">../../<xsl:choose>
      <xsl:when test="'seppo.cgi/search/?q=' = substring($self, 1, 24)"/>
      <xsl:when test="'//' = translate($self, 'abcdefghijklmnopqrstuvwxyz0123456789-', '')"/>
      <xsl:otherwise>../</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="xml_base" select="normalize-space($xml_base_absolute)"/>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'themes/current')"/>
  <xsl:variable name="cgi_base" select="concat($xml_base,'seppo.cgi')"/>

  <xsl:variable name="perso" select="document(concat($xml_base,'activitypub/actor.xml'))/rdf:RDF/as:Person"/>

  <xsl:template match="/">
    <!--
      Do not set a class="logged-out" initially, but do via early JavaScript.
      If JavaScript is off, we need mixture between logged-in and -out.
    -->
    <html xmlns="http://www.w3.org/1999/xhtml" data-xml-base-pub="{$xml_base_pub}" lang="en" xml:lang="en" class="script-inactive">
      <xsl:if test="234 &lt; count(a:feed/a:category)">
        <xsl:attribute name="class">manytags</xsl:attribute>
      </xsl:if>
      <xsl:call-template name="head"/>

      <body>
        <xsl:apply-templates select="a:feed|a:entry" mode="root"/>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="head">
    <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
      <link rel="icon" type="image/jpg" href="{$xml_base}me-avatar.jpg"/>
      <link rel="stylesheet" type="text/css" href="{$skin_base}/awesomplete.css"/>
      <link rel="stylesheet" type="text/css" href="{$skin_base}/style.css"/>
      <script src="{$skin_base}/posts.js"></script>
      <script src="{$skin_base}/awesomplete.js"></script>
      <!-- script src="{$skin_base}/../../live.js"></script -->

      <link rel="alternate" type="application/atom+xml" href="."/>
      <link rel="self" type="application/xhtml+xml" href="."/>

      <title><xsl:value-of select="a:*/a:title"/></title>
    </head>
  </xsl:template>

  <xsl:template match="a:feed" mode="root">
    <noscript><p>JavaScript deactivated, almost fully functional, but <em>nicer</em> if on.</p></noscript>

    <xsl:call-template name="links_commands"/>

    <!-- h1><xsl:value-of select="a:title"/></h1 -->

    <p id="tags" class="categories">
      <xsl:variable name="countMax">
        <!-- https://stackoverflow.com/a/17966412 -->
        <xsl:for-each select="a:category">
          <xsl:sort select="@label" data-type="number" order="descending"/>
          <xsl:if test="position() = 1"><xsl:value-of select="@label"/></xsl:if>
        </xsl:for-each>
      </xsl:variable>

      <xsl:variable name="labelsDesc">
        <xsl:for-each select="a:category">
          <xsl:sort select="@label" order="descending"/>
          <xsl:value-of select="@label"/>
        </xsl:for-each>
      </xsl:variable>
      <xsl:for-each select="a:category[@label >= 1]">
        <xsl:sort select="@term" order="ascending"/>
        <!-- not log, just linear, similar to https://github.com/sebsauvage/Shaarli/blob/master/index.php#L1254 -->
        <xsl:variable name="size" select="8 + 40 * @label div $countMax"/>
        <a style="font-size:{$size}pt" href="{$cgi_base}/search/?q=%23{@term}+" class="tag" data-count="{@label}"><span class="label"><xsl:value-of select="@term"/></span><span style="font-size:8pt">&#160;(<span class="count"><xsl:value-of select="@label"/></span>)</span></a><xsl:text> </xsl:text>
      </xsl:for-each>
    </p>

    <ol id="entries" data-level="0" class="even">
      <xsl:apply-templates select="a:entry"/>
    </ol>

    <xsl:call-template name="prev-next"/>

    <xsl:call-template name="footer"/>
  </xsl:template>

  <xsl:template name="prev-next">
    <xsl:if test="a:link[@rel='first'] or a:link[@rel='last']">
      <table class="prev-next noshade">
        <tbody>
          <tr>
            <xsl:variable name="self" select="a:link[@rel='self']"/>
            <td class="text-left" aria-label="First" title="most recent">
              <xsl:variable name="link" select="a:link[@rel='first']"/>
              <a href="{$xml_base_relative}{$link/@href}" class="first btn"><xsl:value-of select="$link/@title"/></a>
            </td>
            <td class="text-center" title="more recent">
              <xsl:variable name="link" select="a:link[@rel='previous']"/>
              <a href="{$xml_base_relative}{$link/@href}" class="previous enabled btn">&#160;<xsl:value-of select="$link/@title"/>&#160;</a>
            </td>
            <td class="text-center" title="current">
              <span class="hidden-xs"></span><xsl:value-of select="$self/@title"/>
            </td>
            <td class="text-center" title="older">
              <xsl:variable name="link" select="a:link[@rel='next']"/>
              <xsl:variable name="disabled"><xsl:if test="not($link)">disabled</xsl:if></xsl:variable>
              <a href="{$xml_base_relative}{$link/@href}" class="previous {$disabled} btn">&#160;<xsl:value-of select="$link/@title"/>&#160;</a>
            </td>
            <td class="text-right" title="oldest">
              <xsl:variable name="link" select="a:link[@rel='last']"/>
              <a href="{$xml_base_relative}{$link/@href}" class="last btn"><xsl:value-of select="$link/@title"/></a>
            </td>
          </tr>
        </tbody>
      </table>
    </xsl:if>
  </xsl:template>

  <xsl:template name="links_commands">
    <div aria-label="Header" id="header">
      <div id="banner">
        <img width="720" height="240" alt="Banner" src="{$xml_base}me-banner.jpg"/>
        <div id="head-grid">
          <div>
            <a class="btn" href="{$xml_base}" rel="first">🏡 <span class="hidden-xs"></span></a>
            <form action="{$cgi_base}/subscribe" class="hidden-logged-in" id="form_subscribe" name="subscribe">
              <input name="handle" required="required" pattern="@?[a-zA-Z0-9._~\-]+@[a-zA-Z0-9._~\-]+"
               placeholder="@me@example.com" title="@me@example.com" />
              <input type="submit" value="Subscribe 🔔" />
            </form>
            <a class="btn hidden-logged-out" href="{$cgi_base}/profile" rel="nofollow">🎭 <span class="hidden-xs">Profile</span></a>
          </div>

          <a id="link_login" href=
          "{$cgi_base}/login?returnurl=.." class="btn hidden-logged-in" rel="nofollow" name=
          "link_login"><span class="hidden-xs">Login</span> 👋</a>

           <a id="link_logout" href=
          "{$cgi_base}/logout" class="btn hidden-logged-out" rel="nofollow" name=
          "link_logout"><span class="hidden-xs">Logout</span> 👋</a>
        </div>

        <div id="subscribe" class="hidden-logged-out">
          <a class="btn" href="{$xml_base}activitypub/subscribed/" rel="nofollow" title="a.k.a. Following">Subscribed 👂</a>
          <a class="btn" href="{$xml_base}activitypub/notify/" rel="nofollow" title="a.k.a. Followers">Notify 📣</a>
          <a class="btn disabled" href="{$xml_base}activitypub/blocked/" rel="nofollow">Blocked 🤐</a>
          <a class="btn" href="{$cgi_base}/health/" rel="nofollow" title="Health">Health ❤️</a>
        </div>
      </div>
      <div class="noshade" id="avatar">
        <a href="{$xml_base}"><img alt="Avatar" src="{$xml_base}me-avatar.jpg"/></a>
      </div>

      <h3 class="noshade" id="name"><a href="{$perso/as:url/@rdf:resource}" title="name"><xsl:value-of select="$perso/as:name"/></a></h3>

      <xsl:variable name="host1" select="$perso/as:url/@rdf:resource"/>
      <xsl:variable name="host0" select="substring-after($host1,'://')"/>
      <xsl:variable name="host" select="substring-before($host0,'/')"/>
      <p class="noshade"><a href="{$perso/as:url/@rdf:resource}" title="preferredUsername">@<span id="preferredUsername"><xsl:value-of select="$perso/as:preferredUsername"/></span>@<xsl:value-of select="$host"/></a></p>

      <xsl:if test="1 or a:link[@rel='first']/@href = a:link[@rel='self']/@href">
        <div class="noshade" id="bio">
          <p class="clickable" title="summary">
            <xsl:call-template name="linefeed2br">
              <xsl:with-param name="string" select="$perso/as:summary"/>
            </xsl:call-template>
          </p>

          <dl>
            <xsl:for-each select="$perso/as:attachment/schema:PropertyValue[schema:value]">
              <dt class="clickable"><xsl:value-of select="as:name"/></dt>
              <dd class="clickable"><xsl:value-of select="schema:value"/></dd>
            </xsl:for-each>
          </dl>
        </div>
      </xsl:if>
      <xsl:comment> https://stackoverflow.com/a/18520870 http://jsfiddle.net/66Ynx/ </xsl:comment>
      <form id="form_search" name="form_search" action="{$cgi_base}/search/">
        <input disabled="disabled" class="disabled awesomplete" tabindex="100" name="q" id="q" value="{@se:searchTerms}" type="text" placeholder="🔍
Search text or #tag or @actor ..." data-multiple="true"/>
      </form>
      <form id="form_post" name="form_post" class="hidden-logged-out noshade" action="{$cgi_base}/post">
        <input tabindex="300" name="post" type="text" placeholder="What to #post? (text note or URL)" autofocus="autofocus"/>
      </form>

      <xsl:call-template name="prev-next"/>
    </div>
  </xsl:template>

  <xsl:template name="footer">
    <p id="footer">
      <a title="Syndicate" href="{$xml_base_absolute}{a:link[@rel='self']/@href}">
        <img alt="Feed" src="{$skin_base}/feed-icon.svg"/>
      </a>
      <xsl:text> </xsl:text>
       <a title="Validate (Atom 1.0)" href="https://validator.w3.org/feed/check.cgi?url={$xml_base_absolute}{a:link[@rel='self']/@href}">
        <img alt="Validity badge (Atom 1.0)" src="{$skin_base}/valid-atom.svg"/>
      </a>
      <!-- <xsl:text> </xsl:text>
      <a href="https://validator.w3.org/check?uri=referer">
        <img alt="Valid XHTML 1.0 Strict" src="{$skin_base}/valid-xhtml10-blue-v.svg"/>
      </a>
      <a href="https://jigsaw.w3.org/css-validator/check/referer?profile=css3&amp;usermedium=screen&amp;warning=2&amp;vextwarning=false&amp;lang=de">
        <img alt="CSS ist valide!" src="{$skin_base}/valid-css-blue-v.svg"/>
      </a>
      -->
      <xsl:text> </xsl:text>
  <!--
      <a href="{a:link[@rel='about']/@href}">
        About<xsl:if test="string-length(a:link[@rel='about']/@href) &lt; 2"><span> link rel='about' missing.</span></xsl:if>
      </a>
      <xsl:text> </xsl:text>
      <a href="{a:link[@rel='license']/@href}">
        <xsl:value-of select="a:link[@rel='license']/@title"/><xsl:if test="string-length(a:link[@rel='license']/@href) &lt; 2"><span> link rel='license' missing.</span></xsl:if>
      </a>
      <xsl:text> </xsl:text>
      <a href="{a:link[@rel='terms-of-service']/@href}">
        § Imprint<xsl:if test="string-length(a:link[@rel='terms-of-service']/@href) &lt; 2"><span> link rel='terms-of-service' missing.</span></xsl:if>
      </a>
      <xsl:text> </xsl:text>
      <a href="{a:link[@rel='privacy-policy']/@href}">
        🤫 Privacy<xsl:if test="string-length(a:link[@rel='terms-of-service']/@href) &lt; 2"><span> link rel='terms-of-service' missing.</span></xsl:if>
      </a>
  -->
      <xsl:text> </xsl:text>
      <a title="Generator" href="https://{a:generator/@uri}">
        <xsl:value-of select="a:generator"/>
      </a>
    </p>
  </xsl:template>


  <xsl:template match="a:entry" mode="root">
    <noscript><p>JavaScript deactivated, almost fully functional, but <em>nicer</em> if on.</p></noscript>

    <xsl:call-template name="links_commands"/>

    <ol id="entries" data-level="0" class="even">
      <xsl:apply-templates select="."/>
    </ol>

    <xsl:call-template name="footer"/>
  </xsl:template>

  <xsl:template match="a:entry">
    <xsl:variable name="link" select="a:link[not(@rel)]/@href"/>
    <xsl:variable name="self" select="a:link[@rel='self']/@href"/>
    <xsl:variable name="id_slash" select="substring-after($self, '/p/')"/>
    <xsl:variable name="id" select="substring-after($self, '#')"/>
    <li class="noshade" id="{$id}" data-id="{a:id}">
      <h3>
        <xsl:if test="media:thumbnail/@url">
          <!-- https://varvy.com/pagespeed/defer-images.html -->
          <!-- img alt="Vorschaubild" data-src="{media:thumbnail/@url}" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" / -->
        </xsl:if>
         <xsl:choose>
          <xsl:when test="$link">
            <a href="{$redirector}{$link}" rel="noopener noreferrer" referrerpolicy="no-referrer"><xsl:value-of select="a:title"/></a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="a:title"/>
          </xsl:otherwise>
        </xsl:choose>
      </h3>
      <xsl:if test="a:summary">
        <h5>
          <xsl:call-template name="linefeed2br">
            <xsl:with-param name="string" select="a:summary"/>
          </xsl:call-template>
        </h5>
      </xsl:if>
      <!-- p class="small text-right"><a><xsl:value-of select="$link"/></a></p -->

      <!-- html content won't work that easy (out-of-the-firebox): https://bugzilla.mozilla.org/show_bug.cgi?id=98168#c140 -->
      <!-- workaround via jquery: http://stackoverflow.com/a/9714567 -->

      <!-- Überbleibsel vom Shaarli Atom Feed raus: -->
      <!-- xsl:value-of select="substring-before(a:content[not(@src)], '&lt;br&gt;(&lt;a href=&quot;https://links.mro.name/?')" disable-output-escaping="yes" / -->
      <xsl:apply-templates select="a:content"/>

      <xsl:if test="a:category">
        <p class="categories">
          <xsl:for-each select="a:category">
            <xsl:sort select="@term"/>
            <xsl:if test="position() != 1">
              <xsl:text>, </xsl:text>
            </xsl:if>
            <a href="{@scheme}{@term}/" class="tag">#<xsl:value-of select="@term"/></a>
          </xsl:for-each>
        </p>
      </xsl:if>
      <p class="footer">
        <xsl:variable name="entry_updated" select="a:updated"/>
        <xsl:variable name="entry_updated_human"><xsl:call-template name="human_time"><xsl:with-param name="time" select="$entry_updated"/></xsl:call-template></xsl:variable>
        <xsl:variable name="entry_published" select="a:published"/>
        <xsl:variable name="entry_published_human"><xsl:call-template name="human_time"><xsl:with-param name="time" select="$entry_published"/></xsl:call-template></xsl:variable>

        <a class="from" title="from" href="{a:author/a:uri}">
          <img class="avatar" alt="" src=""/>
          <xsl:value-of select="a:author/a:name"/>
        </a>
        <xsl:text> * </xsl:text>
        <a class="time" title="last: {$entry_updated_human}" href="{a:link[@rel='self']/@href}"><xsl:value-of select="$entry_published_human"/> 🔗</a>
        <xsl:if test="$link">
          <xsl:text> * </xsl:text>
          <a href="{$archive}{$link}" rel="noopener noreferrer" referrerpolicy="no-referrer">@archive.org</a>
        </xsl:if>
        <span class="hidden-logged-out">
          <xsl:text> * </xsl:text>
          <a href="{$cgi_base}/edit?id={translate(a:id,'#','$')}" rel="nofollow">Edit</a><xsl:text> </xsl:text>
          <!-- a href="{$xml_base}{a:link[@rel='edit']/@href}" rel="nofollow">Edit</a><xsl:text> </xsl:text -->
        </span>
        <xsl:text> * </xsl:text>
        <a href="{$cgi_base}/replies?id={a:id}">Replies</a>
      </p>
      <!-- iframe data-src="{$cgi_base}/replies?id={a:id}"/ -->
    </li>
  </xsl:template>

  <xsl:template match="a:content[not(@type) or @type = 'text']">
    <xsl:if test="string-length(.) &gt; 0">
      <p class="clickable rendered type-text">
        <xsl:call-template name="linefeed2br">
          <xsl:with-param name="string" select="."/>
        </xsl:call-template>
      </p>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
