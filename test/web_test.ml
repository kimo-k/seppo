open Seppo_lib

let test_regexp () =
  let rx = Str.regexp {|^[^\n\t ]\([^\n\t]+[^\n\t ]\)?$|} in
  assert (Str.string_match rx "a" 0);
  assert true

let test_markup_xml () =
  [
    `Start_element (("", "foo"), []);
    `Start_element (("", "bar"), []);
    `Start_element (("", "baz"), []);
    `End_element;
    `End_element;
    `End_element;
  ]
  |> Markup.of_list |> Markup.pretty_print |> Markup.write_xml
  |> Markup.to_string
  |> Assert2.equals_string "foo" "<foo>\n <bar>\n  <baz/>\n </bar>\n</foo>\n"

let test_login () =
  let tit = "> U \" h & ' u <"
  and tok = "ff13e7eaf9541ca2ba30fd44e864c3ff014d2bc9"
  and ret = "retu"
  and att n v = (("", n), v)
  and elm name atts = `Start_element (("", name), atts) in
  [
    `Xml
      {
        Markup.version = "1.0";
        encoding = Some "utf-8";
        standalone = Some false;
      };
    `PI
      ("xml-stylesheet", "type='text/xsl' href='./themes/current/do=login.xslt'");
    `Comment
      "\n\
      \  must be compatible with \
       https://code.mro.name/mro/Shaarli-API-test/src/master/tests/test-post.sh\n\
      \  \
       https://code.mro.name/mro/ShaarliOS/src/1d124e012933d1209d64071a90237dc5ec6372fc/ios/ShaarliOS/API/ShaarliCmd.m#L386\n";
    elm "html" [ att "xmlns" "http://www.w3.org/1999/xhtml" ];
    elm "head" [];
    elm "title" [];
    `Text [ tit ];
    `End_element;
    `End_element;
    elm "body" [];
    elm "form" [ att "method" "post" ];
    elm "input" [ att "name" "login"; att "type" "text" ];
    `End_element;
    elm "input" [ att "name" "password"; att "type" "password" ];
    `End_element;
    elm "input" [ att "name" "longlastingsession"; att "type" "checkbox" ];
    `End_element;
    elm "input" [ att "name" "token"; att "type" "hidden"; att "value" tok ];
    `End_element;
    elm "input" [ att "name" "returnurl"; att "type" "hidden"; att "value" ret ];
    `End_element;
    elm "input" [ att "value" "Login"; att "type" "submit" ];
    `End_element;
    `End_element;
    `End_element;
  ]
  |> Markup.of_list |> Markup.pretty_print |> Markup.write_xml
  |> Markup.to_string |> String.length
  |> Assert2.equals_int "foo" 842

let test_cookie () =
  Web.MyCookie.name |> Assert2.equals_string "c0" "#Seppo!";
  (match "(5:seppi25:2022-10-03T01:02:03+01:00)"
         |> Web.MyCookie.decode with
  | Ok (Auth.Uid uid, _t) ->
    uid |> Assert2.equals_string "c1" "seppi"
  | Error e -> e |> Assert2.equals_string "c2" "");
  (Auth.Uid "seppa", Ptime.epoch)
  |> Web.MyCookie.encode
  |> Assert2.equals_string "c3" "(5:seppa25:1970-01-01T00:00:00-00:00)";
  (match (Auth.Uid "seppu", Ptime.epoch)
         |> Web.MyCookie.encode
         |> Web.MyCookie.decode with
  | Ok (Auth.Uid uid, _t) ->
    uid |> Assert2.equals_string "c4" "seppu"
  | Error e -> e |> Assert2.equals_string "c2" "");
  assert true

let test_csrf_token () =
  let fu () = () in
  let v = "f19a65cecdfa2971afb827bc9413eb7244e469a8" in
  let f = ([("token", [ v ])], ()) in
  match Web.check_token fu v f with
  | Ok (t, _) -> Assert2.equals_string "shaarli_test:test_csrf_token 10" v t
  | Error (s,_,_) ->
    s |> Cohttp.Code.code_of_status |> Assert2.equals_int "shaarli_test:test_csrf_token 20" 200

let test_date () =
  let d x = x
            |> Option.value ~default:Web.Post.epoch_shaarli
            |> Ptime.to_rfc3339
  in
  "20230927_125036" |> Web.Post.s2d |> d |> Assert2.equals_string "web_test.date" "2023-09-27T12:50:36-00:00"

let test_bookmarklet () =
  let s = Option.value ~default:"" in
  let b s = if s then "yes" else "no" in
  let d s = s |> Option.value ~default:Ptime.min |> Ptime.to_rfc3339 in
  let u x = x |> Option.value ~default:Uri.empty |> Uri.to_string in
  let s' x= x in
  let l = String.concat " " in
  let now = ((2023,9,27),((14,45,42),2*60*60))
            |> Ptime.of_date_time in
  let emp = Web.Post.empty in
  let emp = {emp with dat = now} in
  let x = {|post=https%3A%2F%2Fwww.heise.de%2F&source=bookmarklet&scrape=no&title=heise+online+-+IT-News%2C+Nachrichten+und+Hintergr%C3%BCnde&tags=heise+online%2C+c%27t%2C+iX%2C+MIT+Technology+Review%2C+Newsticker%2C+Telepolis%2C+Security%2C+Netze&image=https%3A%2F%2Fheise.cloudimg.io%2Fbound%2F1200x1200%2Fq85.png-lossy-85.webp-lossy-85.foil1%2F_www-heise-de_%2Ficons%2Fho%2Fopengraph%2Fopengraph.png&description=News+und+Foren+zu+Computer%2C+IT%2C+Wissenschaft%2C+Medien+und+Politik.+Preisvergleich+von+Hardware+und+Software+sowie+Downloads+bei+Heise+Medien.|} in
  let r : Web.Post.t = x
                       |> Uri.query_of_encoded
                       |> List.fold_left Web.Post.sift_bookmarklet_get emp in
  r.scrape |> b |> Assert2.equals_string "web.bookmarklet 010" "yes";
  r.source |> s |> Assert2.equals_string "web.bookmarklet 020" "bookmarklet";
  r.dat    |> d |> Assert2.equals_string "web.bookmarklet 030" "2023-09-27T12:45:42-00:00";
  r.url    |> u |> Assert2.equals_string "web.bookmarklet 040" "https://www.heise.de/";
  r.tit    |> s |> Assert2.equals_string "web.bookmarklet 050" "heise online - IT-News, Nachrichten und Hintergründe";
  r.dsc    |> s |> Assert2.equals_string "web.bookmarklet 060" "News und Foren zu Computer, IT, Wissenschaft, Medien und Politik. Preisvergleich von Hardware und Software sowie Downloads bei Heise Medien.";
  r.tag    |> l |> Assert2.equals_string "web.bookmarklet 070" "heise online, c't, iX, MIT Technology Review, Newsticker, Telepolis, Security, Netze";
  r.pri    |> b |> Assert2.equals_string "web.bookmarklet 080" "no";
  assert (r.sav    |> Option.is_none);
  r.can    |> s |> Assert2.equals_string "web.bookmarklet 100" "";
  r.tok    |> s'|> Assert2.equals_string "web.bookmarklet 110" "";
  r.ret    |> u |> Assert2.equals_string "web.bookmarklet 120" "";
  r.img    |> u |> Assert2.equals_string "web.bookmarklet 130" "https://heise.cloudimg.io/bound/1200x1200/q85.png-lossy-85.webp-lossy-85.foil1/_www-heise-de_/icons/ho/opengraph/opengraph.png";
  let x = {|post=Some #text 🐫|} in
  let r : Web.Post.t = x
                       |> Uri.query_of_encoded
                       |> List.fold_left Web.Post.sift_bookmarklet_get emp in
  r.scrape |> b |> Assert2.equals_string "web.bookmarklet 210" "no";
  r.source |> s |> Assert2.equals_string "web.bookmarklet 220" "";
  r.dat    |> d |> Assert2.equals_string "web.bookmarklet 230" "2023-09-27T12:45:42-00:00";
  r.url    |> u |> Assert2.equals_string "web.bookmarklet 240" "";
  r.tit    |> s |> Assert2.equals_string "web.bookmarklet 250" "Some #text 🐫";
  r.dsc    |> s |> Assert2.equals_string "web.bookmarklet 260" "";
  r.tag    |> l |> Assert2.equals_string "web.bookmarklet 270" "";
  r.pri    |> b |> Assert2.equals_string "web.bookmarklet 280" "no";
  assert (r.sav    |> Option.is_none);
  r.can    |> s |> Assert2.equals_string "web.bookmarklet 300" "";
  r.tok    |> s'|> Assert2.equals_string "web.bookmarklet 310" "";
  r.ret    |> u |> Assert2.equals_string "web.bookmarklet 320" "";
  r.img    |> u |> Assert2.equals_string "web.bookmarklet 330" ""


let test_post () =
  let x = "?lf_linkdate=20210913_134542&token=f19a65cecdfa2971afb827bc9413eb7244e469a8&returnurl=&lf_image=&lf_url=http://example.com&lf_title=title&lf_description=body%20%23tags&save_edit=Save" in
  let s = Option.value ~default:"" in
  let b s = if s then "yes" else "no" in
  let d s = s |> Option.value ~default:Web.Post.epoch_shaarli |> Ptime.to_rfc3339 in
  let u x = x |> Option.value ~default:Uri.empty |> Uri.to_string in
  let l = String.concat " " in
  let s' x = x in

  let r : Web.Post.t = x
                       |> Uri.of_string
                       |> Uri.query
                       |> List.fold_left Web.Post.sift_post Web.Post.empty in
  r.scrape |> b |> Assert2.equals_string "web_test.post 010" "no";
  r.source |> s |> Assert2.equals_string "web_test.post 020" "";
  r.dat    |> d |> Assert2.equals_string "web_test.post 030" "2021-09-13T13:45:42-00:00";
  r.url    |> u |> Assert2.equals_string "web_test.post 040" "http://example.com";
  r.tit    |> s |> Assert2.equals_string "web_test.post 050" "title";
  r.dsc    |> s |> Assert2.equals_string "web_test.post 060" "body #tags";
  r.tag    |> l |> Assert2.equals_string "web_test.post 070" "";
  r.pri    |> b |> Assert2.equals_string "web_test.post 080" "no";
  (match r.sav with | Some Save -> "Save"| _ -> "Fail") |> Assert2.equals_string "web_test.post 090" "Save";
  r.can    |> s |> Assert2.equals_string "web_test.post 100" "";
  r.tok    |> s'|> Assert2.equals_string "web_test.post 110" "f19a65cecdfa2971afb827bc9413eb7244e469a8";
  r.ret    |> u |> Assert2.equals_string "web_test.post 120" "";
  r.img    |> u |> Assert2.equals_string "web_test.post 130" ""

module Actor = struct
  let test_basic () =
    Logr.info (fun m -> m "%s.%s" "Web.Actor" "basic");
    let s = {|token=68f4cf03-8f2d-491c-a954-bd8118f93c01&id=https%3A%2F%2Falpaka.social%2Fusers%2Ftraunstein&inbox=https%3A%2F%2Falpaka.social%2Fusers%2Ftraunstein%2Finbox&~notify=no&~subscribe=yes&~block=no&notify=on|} in
    let f = s |> Http.Form.of_string in
    f |> List.length |> Assert2.equals_int "web.actor.basic 10" 7;
    f |> List.assoc "token"      |> String.concat "|" |> Assert2.equals_string "web.actor.basic 20" "68f4cf03-8f2d-491c-a954-bd8118f93c01";
    f |> List.assoc "id"         |> String.concat "|" |> Assert2.equals_string "web.actor.basic 30" "https://alpaka.social/users/traunstein";
    f |> List.assoc "inbox"      |> String.concat "|" |> Assert2.equals_string "web.actor.basic 40" "https://alpaka.social/users/traunstein/inbox";
    f |> List.assoc "~notify"    |> String.concat "|" |> Assert2.equals_string "web.actor.basic 50" "no";
    f |> List.assoc "~subscribe" |> String.concat "|" |> Assert2.equals_string "web.actor.basic 60" "yes";
    f |> List.assoc "~block"     |> String.concat "|" |> Assert2.equals_string "web.actor.basic 70" "no";
    f |> List.assoc "notify"     |> String.concat "|" |> Assert2.equals_string "web.actor.basic 80" "on";
    let switch k v' v =
      if v' = v
      then None
      else (
        Logr.debug (fun m -> m "field %s: %s" k (v |> As2.No_p_yes.to_string));
        Some k) in
    let form_switch_folder k_of_old f_switch form init (k_old,v_old) =
      match k_old |> k_of_old with
      | None   -> init
      | Some k ->
        let v = match form |> List.assoc_opt k with
          | None
          | Some ["no"] -> As2.No_p_yes.No
          | _           -> As2.No_p_yes.Yes in
        let v_old = match v_old with
          | ["no"] -> As2.No_p_yes.No
          | _      -> As2.No_p_yes.Yes in
        match f_switch k v_old v with
        | None -> init
        | Some x -> x :: init in
    f
    |> List.fold_left (form_switch_folder (St.after ~prefix:"~") switch f) []
    |> String.concat "|"
    |> Assert2.equals_string "web.actor.basic 90" "subscribe|notify"
end

let () =
  Unix.chdir "../../../test/";
  test_regexp ();
  test_markup_xml ();
  test_login ();
  test_cookie ();
  test_csrf_token ();
  test_date ();
  test_bookmarklet ();
  test_post ();
  Actor.test_basic ();
  assert true
