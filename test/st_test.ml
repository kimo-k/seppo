(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)
open Seppo_lib

let test_dirname () =
  "./foo/../bar/baz" |> Filename.dirname |> Assert2.equals_string "St.dirname 10" "./foo/../bar"

let test_updir () =
  "" |> St.updir |> Assert2.equals_string "St.updir 10" "";
  "foo" |> St.updir |> Assert2.equals_string "St.updir 20" "";
  "foo/" |> St.updir |> Assert2.equals_string "St.updir 30" "../";
  "/foo/" |> St.updir |> Assert2.equals_string "St.updir 31" "../";
  "foo/bar" |> St.updir |> Assert2.equals_string "St.updir 30" "../";
  "/foo" |> St.updir |> Assert2.equals_string "St.updir 40" "";
  "/foo/bar" |> St.updir |> Assert2.equals_string "St.updir 50" "../";
  "/foo/bar/" |> St.updir |> Assert2.equals_string "St.updir 50" "../../";
  "/.well-known/webfinger/.htaccess" |> St.updir |> Assert2.equals_string "St.updir 50" "../../";
  ".well-known/webfinger/.htaccess" |> St.updir |> Assert2.equals_string "St.updir 50" "../../"

let test_before () =
  "uhu/index.xml"
  |> St.before ~suffix:"index.xml"
  |> Option.get
  |> Assert2.equals_string "test_st.test_before 10" "uhu/"

let () =
  test_dirname ();
  test_updir ();
  test_before ();
