#!/bin/sh
#set -e
cd "$(dirname "${0}")"

# readonly cgi="_build/seppo-Linux-x86_64-0.1.cgi"
readonly cgi="$(pwd)/_build/default/bin/seppo.exe"

mkdir -p /tmp/seppo/
cd /tmp/seppo/

"${cgi}" dot \
  | dot -Tsvg -o seppo.svg

rm \
  app/etc/id_rsa.priv.pem \
  activitypub/id_rsa.pub.pem \
  activitypub/index.json

"${cgi}" activitypub

ls -Altr \
  app/etc/id_rsa.priv.pem \
  activitypub/* \
	.well-known/webfinger/*

#tree
exit 0

export PATH_INFO="/activitypub/monitor.txt"
export REQUEST_METHOD="GET"
export SERVER_NAME="example.com"
export SERVER_PORT="443"
export REMOTE_ADDR="127.0.0.1"
export SCRIPT_NAME="seppo.cgi"

echo "${cgi}${PATH_INFO}"

# sh app/etc/keygen.sh
"${cgi}"

