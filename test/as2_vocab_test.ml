
open Seppo_lib

(* diaspora profile json e.g.

   https://pod.diaspora.software/u/hq
   https://pod.diaspora.software/people/7bca7c80311b01332d046c626dd55703
*)

let test_person () =
  let empty : As2_vocab.Types.person = {
    id                          = Uri.empty;
    inbox                       = Uri.empty;
    outbox                      = Uri.empty;
    followers                   = None;
    following                   = None;
    attachment                  = [];
    discoverable                = false;
    icon                        = None;
    image                       = None;
    manually_approves_followers = false;
    name                        = None;
    name_map                    = [];
    preferred_username          = None;
    preferred_username_map      = [];
    public_key                  = {
      id                        = Uri.empty;
      owner                     = None;
      pem                       = "";
      signatureAlgorithm        = None;
    };
    summary                     = None;
    summary_map                 = [];
    url                         = None;
  } in
  let base = (Uri.of_string "https://example.com/su/") in
  let context = As2_vocab.Constants.ActivityStreams.und in
  {empty with id=Uri.make ~path:"id/" ()}
  |> As2_vocab.Encode.person ~context ~base
  |> Ezjsonm.value_to_string ~minify:false
  |> Assert2.equals_string __LOC__ {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "Person",
  "id": "https://example.com/su/id/",
  "inbox": "https://example.com/su/",
  "outbox": "https://example.com/su/",
  "publicKey": {
    "@context": [
      {
        "@language": null
      }
    ],
    "id": "https://example.com/su/",
    "publicKeyPem": ""
  },
  "manuallyApprovesFollowers": false,
  "discoverable": false,
  "attachment": []
}|};
  let p = {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "Person",
  "id": "https://example.com/su/id/",
  "inbox": "https://example.com/su/",
  "outbox": "https://example.com/su/",
  "publicKey": {
    "id": "https://example.com/su/",
    "owner": "https://example.com/su/",
    "publicKeyPem": ""
  },
  "manuallyApprovesFollowers": false,
  "discoverable": false,
  "attachment": []
}|}
          |> Ezjsonm.value_from_string
          |> As2_vocab.Decode.person
          |> Result.get_ok in
  p.id
  |> Uri.to_string
  |> Assert2.equals_string "as2_vocab_test test_person 20" "https://example.com/su/id/"

let test_note_decode () =
  Logr.info (fun m -> m "%s.%s" "As2_vocab" "note_decode");
  let j = File.in_channel "data/note.mastodon.json" Ezjsonm.from_channel in
  let n = j |> As2_vocab.Decode.note |> Result.get_ok in
  n.id |> Uri.to_string |> Assert2.equals_string "as2_vocab.note_decode 10" "https://digitalcourage.social/users/mro/statuses/111403080326863922";
  match n.in_reply_to with
  | [u] -> u |> Uri.to_string |> Assert2.equals_string "as2_vocab.note_decode 20" "https://chaos.social/users/qyliss/statuses/111403054651938519"
  | _ -> failwith "none"

let test_webfinger_sunshine () =
  let q = File.in_channel "data/webfinger.mini.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.Webfinger.query_result
          |> Result.get_ok in
  q.subject |> Assert2.equals_string "ap_vocab_test test_a 10" "acct:ursi@example.com";
  q.links |> List.length |> Assert2.equals_int "ap_vocab_test test_a 20" 3;
  let q = File.in_channel "data/webfinger.zap.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.Webfinger.query_result
          |> Result.get_ok in
  q.subject |> Assert2.equals_string "as2_vocab_test test_sunshine_atom 10" "acct:mike@macgirvin.com";
  q.links |> List.length |> Assert2.equals_int "as2_vocab_test test_sunshine_atom 20" 3;
  let q = File.in_channel "data/webfinger.atom.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.Webfinger.query_result
          |> Result.get_ok in
  q.subject |> Assert2.equals_string "as2_vocab_test test_sunshine_atom 10" "acct:ursi@example.com";
  q.links |> List.length |> Assert2.equals_int "as2_vocab_test test_sunshine_atom 20" 3

let test_profile_sunshine () =
  let q = File.in_channel "data/profile.mini.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.name |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "Yet Another #Seppo! 🌻";
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "ursi";
  q.attachment |> List.length |> Assert2.equals_int "" 0;
  let q = File.in_channel "data/profile.mast.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  (match q.attachment with
   | [a;b;_] ->
     a.name |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 40" "Support";
     b.value |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 50" {|<a href="https://seppo.social">Seppo.Social</a>|};
   | _ -> Assert2.equals_int "" 0 1
  );
  q.image |> Option.get |> Uri.to_string |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 60" "https://example.com/me-banner.jpg";
  "" |> Assert2.equals_string "test_profile_sunshine" "";
  let q = File.in_channel "data/profile.akkoma.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.name |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "Kinetix";
  let q = File.in_channel "data/profile.gnusocial.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "diogo";
  let q = File.in_channel "data/profile.lemmy.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "nutomic";
  let q = File.in_channel "data/profile.mast.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "ursi";
  let q = File.in_channel "data/profile.peertube.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "edps";
  let q = File.in_channel "data/profile.zap.json" Ezjsonm.from_channel
          |> As2_vocab.Activitypub.Decode.person
          |> Result.get_ok in
  q.preferred_username |> Option.get |> Assert2.equals_string "as2_vocab_test test_profile_sunshine 10" "mike";
  assert true

let test_encode () =
  let minify = false in
  let base = Uri.of_string "http://example.com/foo/" in
  let module E = Decoders_ezjsonm.Encode in
  E.encode_string E.obj [("k", `String "v")]
  |> Assert2.equals_string "as2_vocab_test/test_encode 10" {|{"k":"v"}|};
  Ezjsonm.value_to_string (E.obj [("k", `String "v")])
  |> Assert2.equals_string "as2_vocab_test/test_encode 20" {|{"k":"v"}|};

  let e : Rfc4287.Entry.t = {
    id         = Uri.make ~path:"a/b/" ?fragment:(Some "c") ();
    in_reply_to = [];
    lang       = Rfc4287.Rfc4646 "de";
    author     = Uri.make ~userinfo:"bar" ();
    title      = "uhu";
    published  = (Rfc3339.T "2023-03-07T01:23:45Z") ;
    updated    = (Rfc3339.T "2023-03-07T01:23:46Z");
    links      = [];
    categories = [
      ((Label (Single "lbl")),(Term (Single "term")),Uri.make ~path:"t/" ());
    ];
    content    = "Das war aber einfach";
  } in
  let n = Ap.Note.of_rfc4287 e in
  let j = n |> As2_vocab.Encode.note ~base in
  j |> Ezjsonm.value_to_string ~minify
  |> Assert2.equals_string "as2_vocab_test/test_encode 30" {|{
  "type": "Note",
  "id": "http://example.com/foo/a/b/#c",
  "actor": "http://example.com/foo/activitypub/actor.jsa",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "http://example.com/foo/activitypub/notify/index.jsa"
  ],
  "mediaType": "text/plain; charset=utf8",
  "content": "Das war aber einfach",
  "sensitive": false,
  "summary": "uhu",
  "published": "2023-03-07T01:23:45Z",
  "tags": [
    {
      "type": "Hashtag",
      "href": "http://example.com/foo/t/term/",
      "name": "#lbl"
    }
  ],
  "url": []
}|};

  let co : 'a As2_vocab.Types.collection_page = {
    id = Uri.of_string "http://example.com/foo/";
    current    = None;
    first      = None;
    is_ordered = true;
    items      = [Ap.Note.mk_create n];
    last       = None;
    next       = None;
    part_of    = None;
    prev       = None;
    total_items= None;
  } in
  let j = As2_vocab.Encode.collection_page ~base
      (As2_vocab.Encode.create ~base
         (As2_vocab.Encode.note ~base))
      co in
  j |> Ezjsonm.value_to_string ~minify
  |> Assert2.equals_string "as2_vocab_test/test_encode 60" {|{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {
      "schema": "http://schema.org#",
      "PropertyValue": "schema:PropertyValue",
      "value": "schema:value",
      "@language": "und"
    }
  ],
  "type": "OrderedCollectionPage",
  "id": "http://example.com/foo/",
  "orderedItems": [
    {
      "type": "Create",
      "id": "http://example.com/foo/a/b/#c/Create",
      "actor": "http://example.com/foo/activitypub/actor.jsa",
      "published": "2023-03-07T01:23:45Z",
      "to": [
        "https://www.w3.org/ns/activitystreams#Public"
      ],
      "cc": [
        "http://example.com/foo/activitypub/notify/index.jsa"
      ],
      "directMessage": false,
      "object": {
        "type": "Note",
        "id": "http://example.com/foo/a/b/#c",
        "actor": "http://example.com/foo/activitypub/actor.jsa",
        "to": [
          "https://www.w3.org/ns/activitystreams#Public"
        ],
        "cc": [
          "http://example.com/foo/activitypub/notify/index.jsa"
        ],
        "mediaType": "text/plain; charset=utf8",
        "content": "Das war aber einfach",
        "sensitive": false,
        "summary": "uhu",
        "published": "2023-03-07T01:23:45Z",
        "tags": [
          {
            "type": "Hashtag",
            "href": "http://example.com/foo/t/term/",
            "name": "#lbl"
          }
        ],
        "url": []
      }
    }
  ]
}|};

  assert true

(* https://www.w3.org/TR/activitystreams-core/#ex17-jsonld *)
let test_ex15_note () =
  let j = File.in_channel "data/note.as2_core.ex15.json" Ezjsonm.value_from_channel in
  (match j with
   | `O [
       "@context", _ ;
       "summary", _ ;
       "type", `String "Create" ;
       "actor",  _ ;
       "object", `O [
         "type", `String "Note" ;
         _]
     ] -> assert true
   | _ -> assert false);
  let p = j
          |> As2_vocab.Decode.(create note)
          |> Result.is_error in
  assert p
  (*
  _p.obj.summary |> Option.get |> Assert2.equals_string "" "";
*)

(* https://github.com/mattjbray/ocaml-decoders *)
type role = Admin | User

type user =
  { lang : string
  ; txt  : role list
  }

let test_example() =
  Logr.info (fun m -> m "%s.%s" "As2_vocab" "example");
  let module My_encoders(E : Decoders.Encode.S) = struct
    open E

    let user : role encoder =
      function
      | Admin -> string "ADMIN"
      | User -> string "USER"

    let user : user encoder =
      fun u ->
      obj
        [ ("name", string u.lang)
        ; ("roles", list user u.txt)
        ]
  end in
  let module E = Decoders_ezjsonm.Encode in
  let module My_ezjson_encoders = My_encoders(Decoders_ezjsonm.Encode) in
  let open My_ezjson_encoders in
  let users =
    [ {lang = "Alice"; txt = [Admin; User]}
    ; {lang = "Bob"; txt = [User]}
    ] in
  E.encode_string E.obj [("users", E.list user users)]
  |> Assert2.equals_string "example 10" {|{"users":[{"name":"Alice","roles":["ADMIN","USER"]},{"name":"Bob","roles":["USER"]}]}|}

type _i18n =
  { lang : string
  ; txt  : string
  }

let test_encode_content_map () =
  Logr.info (fun m -> m "%s.%s" "As2_vocab" "encode_content_map");
  let l = [("a","A");("b","B")] in
  let module E = Decoders_ezjsonm.Encode in
  let j = l |> List.map (fun (k,v) -> (k,E.string v)) in
  E.encode_string E.obj j
  |> Assert2.equals_string "encode_content_map 10" {|{"a":"A","b":"B"}|}

let test_decode_content_map () =
  Logr.info (fun m -> m "%s.%s" "As2_vocab" "decode_content_map");
  let s = Ezjsonm.value_from_string {|{"a":"A","b":"B"}|} in
  let module D = Decoders_ezjsonm.Decode in
  let l = D.key_value_pairs D.string s
          |> Result.get_ok in
  (match l with
   | [("a","A");("b","B")] -> ()
   | _ ->  "" |> Assert2.equals_string "decode_content_map 20" {|{"a":"A","b":"B"}|});
  let s = Ezjsonm.value_from_string {|{"contentMap":{"a":"A","b":"B","b":"C"}}|} in
  let l = D.field "contentMap" (D.key_value_pairs D.string) s
          |> Result.get_ok in
  match l with
  | [("a","A");("b","B");("b","C")] -> ()
  | _ ->  "" |> Assert2.equals_string "decode_content_map 10" {|{"a":"A","b":"B"}|}

let test_decode_natur () =
  (* https://codeberg.org/seppo/seppo/issues/5 *)
  Logr.info (fun m -> m "%s.%s" "As2_vocab" "decode_natur");
  let j = File.in_channel "data/2024-01-14-173613-actor.natur.json" Ezjsonm.from_channel in
  let e = j |> As2_vocab.Decode.person |> Result.get_error in
  let s = e |> Decoders_ezjsonm.Decode.string_of_error in
  s |> Assert2.equals_string "decode_natur 10" {|Expected an object with an attribute "publicKey", but got
{"id":"https://dev.rdf-pub.org/d613b246-8984-4654-903d-8d44143aca40","type":"Person","inboxSparql":"https://dev.rdf-pub.org/d613b246-8984-4654-903d-8d44143aca40/inbox/sparql","rdfpub:oauth2Issuer":"https://login.m4h.network/auth/realms/LOA","rdfpub:oauth2IssuerPreferredUserName":"max@login.m4h.network","rdfpub:oauth2IssuerUserId":"1813bdc1-152c-4c27-92a6-6cdfe401ef3d@login.m4h.network","outboxSparql":"https://dev.rdf-pub.org/d613b246-8984-4654-903d-8d44143aca40/outbox/sparql","identifier":"52ff7eb2-0b7c-4388-9894-b40a27714c1b","version":{"type":"xsd:integer","@value":"1"},"owl:sameAs":{"id":"https://dev.rdf-pub.org/05a75688-c517-4ae1-842c-5da3d8460627"},"inbox":"https://dev.rdf-pub.org/d613b246-8984-4654-903d-8d44143aca40/inbox","endpoints":{"oauthAuthorizationEndpoint":"https://dev.rdf-pub.org/oauth/oauthAuthorizationEndpoint","oauthTokenEndpoint":"https://dev.rdf-pub.org/oauth/oauthTokenEndpoint"},"name":"max","outbox":"https://dev.rdf-pub.org/d613b246-8984-4654-903d-8d44143aca40/outbox","published":"2024-01-14T15:59:42.102+01:00","@context":["https://schema.org/docs/jsonldcontext.json","https://rdf-pub.org/schema/rdf-pub-context.json","https://www.w3.org/ns/activitystreams"]}|}

let test_decode_sharkey() =
  (* https://joinsharkey.org *)
  Logr.info (fun m -> m "%s.%s" "As2_vocab" "decode_sharkey");
  let j = File.in_channel "data/2024-02-21-222112-actor.sharkey.json" Ezjsonm.from_channel in
  let p = j |> As2_vocab.Decode.person |> Result.get_ok in
  p.name
  |> Option.value ~default:"-"
  |> Assert2.equals_string "decode_sharkey 10" {|-|};
  (File.in_channel "data/2024-02-22-100617-actor.sharkey.json" Ezjsonm.from_channel
   |> As2_vocab.Decode.person
   |> Result.get_ok)
  .name
  |> Option.value ~default:"-"
  |> Assert2.equals_string "decode_sharkey 20" {|wakest the shark possum|}

let () =
  Unix.chdir "../../../test/";
  test_person ();
  test_note_decode ();
  test_webfinger_sunshine ();
  test_profile_sunshine ();
  test_encode ();
  test_ex15_note ();
  test_example ();
  test_encode_content_map ();
  test_decode_content_map ();
  test_decode_natur ();
  test_decode_sharkey();
  assert true
