(2:id7:asdbx8u4:lang2:en5:title53:#Announce Seppo.Social v0.1 and Request for Comments.6:author0:9:published25:2023-02-11T11:07:23+01:007:updated25:2023-02-11T11:07:23+01:005:links((4:href57:https://seppo.social/en/downloads/seppo-Linux-x86_64-0.1/))10:categories((5:label11:ActivityPub4:term11:ActivityPub6:scheme0:)(5:label8:Announce4:term8:Announce6:scheme0:)(5:label9:Fediverse4:term9:Fediverse6:scheme0:)(5:label5:Media4:term5:Media6:scheme0:)(5:label14:permacomputing4:term14:permacomputing6:scheme0:)(5:label5:Seppo4:term5:Seppo6:scheme0:)(5:label6:Social4:term6:Social6:scheme0:)(5:label9:webfinger4:term9:webfinger6:scheme0:))7:content635:I am happy to announce the premiere release of #Seppo!, Personal #Social #Media under funding of NLnet.nl.

Find it at https://Seppo.Social/downloads/

It has no notable user facing #ActivityPub features so far, but

- easy setup of instance & account,
- #webfinger discoverability (from e.g. mastodon search),
- a welcoming, long-term reliable website.

I made this embarrassingly limited release to build awareness for low-barrier-entry internet services in general and especially in the field of personal communication as well as letting the #fediverse and #permacomputing communities know.

Your comments are very much appreciated.)