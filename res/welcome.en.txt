#Seppo! - Personal Social Web at your service.

Before we start, there has to be a word of warning, though:

  The internet — much like real life — isn't all friendly participants. Written
  text is prone to misunderstandings. All writing can and will be used against
  you. You have the right to remain silent.

So be forgiving of what reaches you and careful of what you send out.

That being settled, you can
- #subscribe 👂 to others to follow their activities,
- #post texts being visible here on the internet and sent to those subscribing you 📣,
- #boost 🔁 or #reply ↩️ to others' posts.

If you feel for the company of peers or want #support 🤞, have a look at
https://seppo.social/support
