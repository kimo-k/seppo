<?xml version="1.0" encoding="UTF-8"?>
<!--
      _  _   ____                         _  
    _| || |_/ ___|  ___ _ __  _ __   ___ | | 
   |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
   |_      _|___) |  __/ |_) | |_) | (_) |_| 
     |_||_| |____/ \___| .__/| .__/ \___/(_) 
                       |_|   |_|             

  Personal Social Web.

  Copyright (C) The #Seppo contributors. All rights reserved.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.

  https://www.w3.org/TR/1999/REC-xslt-19991116
  https://www.w3.org/TR/1999/REC-xpath-19991116
-->
<xsl:stylesheet
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:h="http://seppo.social/2023/health#"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:schema="http://www.w3.org/2001/XMLSchema#"
  xmlns:seppo="http://seppo.social/2023/ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="h rdf schema seppo"
  version="1.0">

  <xsl:output
    method="html"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/>

  <xsl:variable name="xml_base">../../</xsl:variable>
  <xsl:variable name="xml_base_pub" select="concat($xml_base,'/o')"/>
  <xsl:variable name="skin_base" select="concat($xml_base,'/themes/current')"/>
  <xsl:variable name="cgi_base" select="concat($xml_base,'seppo.cgi')"/>

  <xsl:template match="/rdf:RDF/rdf:Description[@rdf:about='']">
    <html xmlns="http://www.w3.org/1999/xhtml" class="logged-in">
    <head>
      <meta name="generator" content="HTML Tidy for HTML5 for FreeBSD version 5.8.0" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0" />
      <link href="{$skin_base}/style.css" rel="stylesheet" type="text/css" />
      <title>❤️ Health</title>
    </head>
    <body>
      <noscript><p>JavaScript deactivated, fully functional, but <em>nicer</em> if on.</p></noscript>
      <div aria-label="Header" id="header">
        <a class="btn" href="{$xml_base}" rel="first">🏡 <span class="hidden-xs"></span></a>
      </div>
      <div class="container">
        <h1>❤️ How is this #Seppo! doing</h1>
        <h2 id="jobs">Job Queue</h2>
        <ul>
          <li>lock file <tt>app/var/run/queue</tt>: <xsl:value-of select="h:q_lock"/></li>
          <li><tt>app/var/spool/job/cur</tt>:  <xsl:value-of select="h:q_cur"/></li>
          <li><tt>app/var/spool/job/err</tt>:  <xsl:value-of select="h:q_err"/></li>
          <li><tt>app/var/spool/job/new</tt>:  <xsl:value-of select="h:q_new"/></li>
          <li><tt>app/var/spool/job/run</tt>:  <xsl:value-of select="h:q_run"/></li>
          <li><tt>app/var/spool/job/tmp</tt>:  <xsl:value-of select="h:q_tmp"/></li>
          <li><tt>app/var/spool/job/wait</tt>: <xsl:value-of select="h:q_wait"/></li>
        </ul>
        <h2 id="federation">Environment &amp; Federation</h2>
        <ul>
          <li><a href="{h:x509_pem_url}">id_rsa.pub.pem</a></li>
          <ul>
          <li>id: <xsl:value-of select="h:x509_id"/></li>
              <li>fingerprint: <xsl:value-of select="h:x509_fingerprint"/></li>
          </ul>
        </ul>
        <h2 id="tools">Tools</h2>
        <form id="http" method="get" action="http">
          <input type="url" name="get" placeholder="send signed http GET"/>
        </form>
        <h2 id="standards">Standards</h2>
        <dl>
          <dt>"The Internet is for End Users" (<a href="https://www.rfc-editor.org/rfc/rfc8890.html">RFC8890</a>)</dt>
          <dd></dd>
          <dt>Web Host Metadata (<a href=
          "https://www.rfc-editor.org/rfc/rfc6415.html">RFC6415</a>)</dt>
          <dd>not implemented, webfinger endpoint is fixed. Misskeyism.</dd>
          <dt>Webfinger (<a href="https://www.rfc-editor.org/rfc/rfc7033.html">RFC7033</a>)</dt>
          <dd><a rel="webfinger" href="/.well-known/webfinger?resource=acct:2023-08-28@dev.seppo.social">/.well-known/webfinger?resource=acct:2023-08-28@dev.seppo.social</a></dd>
          <dt>Signing Http Messages (<a href=
          "https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12">draft-cavage-http-signatures-12</a>)</dt>
          <dd>all requests are signed with an initially generated RSA key. Rotating breaks followers.
          See <a href="../activitypub/profile.jlda#publicKey">the public key PEM</a> embedded in the
          profile..</dd>
          <dt><a href="https://w3.org/TR/activitystreams-core">ActivityStreams 2.0 (json
          flavour)</a></dt>
          <dt><a href="https://w3.org/TR/activitypub/">ActivityPub</a></dt>
          <dd>Actor, Outbox, Inbox, Followers, Following, Create Note, Follow</dd>
          <dt>security.txt <a href="https://www.rfc-editor.org/rfc/rfc9116">RFC9116</a></dt>
          <dd><a href="/.well-known/security.txt">/.well-known/security.txt</a></dd>
          <dt>CGI (<a href="https://www.rfc-editor.org/rfc/rfc3875.html">RFC3875</a>)</dt>
          <dd>generate static files for reading</dd>
          <dt>Atom Feed (<a href="https://www.rfc-editor.org/rfc/rfc4287">RFC4287</a>, <a href=
          "https://www.rfc-editor.org/rfc/rfc4685">RFC4685</a> &amp; <a href=
          "https://www.rfc-editor.org/rfc/rfc5005">RFC5005</a>)</dt>
          <dd><a href="../o/p/index.xml">../o/p/index.xml</a></dd>
          <dt><a href="https://www.w3.org/TR/xslt-10/">XSLT 1.0</a></dt>
          <dd>client-side turn the atom feeds into xhtml (without javascript)</dd>
          <dt><a href="http://www.w3.org/TR/xhtml1/">XHTML 1.1 strict</a></dt>
          <dd>robust and reliable page rendering</dd>
          <dt><a href="https://www.w3.org/TR/CSS/">CSS</a></dt>
          <dd>layout and (<a href="https://oklch.com/#79.27,0.171,70.67,100">OKLCH</a>) colors, dark mode</dd>
          <dt><a href="https://projects.verou.me/awesomplete/">awesomeplete</a></dt>
          <dt><a href="http://cr.yp.to/cdb/cdb.txt">cdb</a></dt>
          <dd>constant database for lookups</dd>
          <dt><a href="https://en.wikipedia.org/wiki/Canonical_S-expressions">canonical
          s-expressions</a></dt>
          <dd>all local data &amp; most config (except the cdbs). <a href="https://cr.yp.to/qmail/guarantee.html">"Don't parse" (djb)</a>.
          Csexp are much like <a href="https://cr.yp.to/proto/netstrings.txt">netstrings</a></dd>
          <dt><a href="https://nodeinfo.diaspora.software/">NodeInfo</a></dt>
          <dd>not implemented, no functional benefits (just statistics), mastodonism.</dd>
        </dl>
        <h2 id="software">Software</h2>
        <dl>
          <dt><a href="https://Seppo.Social">Seppo.Social</a></dt>
          <dt><a href="https://seppo.social/support/">Seppo.Social/support</a></dt>
          <dt><a href="https://seppo.social/downloads/">Seppo.Social/downloads</a></dt>
          <dt><a href="https://seppo.social/development/">Seppo.Social/development</a></dt>
        </dl>
      </div>
    </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
