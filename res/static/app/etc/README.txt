     _  _   ____                         _  
   _| || |_/ ___|  ___ _ __  _ __   ___ | | 
  |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
  |_      _|___) |  __/ |_) | |_) | (_) |_| 
    |_||_| |____/ \___| .__/| .__/ \___/(_) 
                      |_|   |_|             
 
Personal Social Web.
 

This directory holds all configuration data only written
by the admin web interface 'seppo.cgi/config/'. 

ban.s           banned IPs,  timing & retries
base.url        web interface base url
credential.s    username and password bcrypt hash
id_rsa.priv.pem personal private key, KEEP SAFE and backup!
profile.s       title, bio, image, languages, etc.

