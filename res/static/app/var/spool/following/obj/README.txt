
Store for received unmodified raw (json) activities. Internal only, not public.

A newly incoming messages is written to

  following/obj/tmp/<sha256>/2023-08-26T123456Z.json
and moved to
  following/obj/new/<sha256>/2023-08-26T123456Z.json
and may be moved to
  following/obj/cur/<sha256>/2023-08-26T123456Z.json

with <sha256> being the hex sha256-hash of the sending actor id.

The objects to keep (e.g. after interactions) are moved to
  following/obj/prm/<sha256>/2023-08-26T123456Z.json
