#!/usr/bin/env dash
set -e
set -u
cd "$(dirname "$0")"

rsync -avPz "$1" "seppo.social:~/seppo.social/tmp/"
ssh seppo.social "sh seppo.social/accept.sh tmp/$(basename "$1")"

exit

how would we deploy to demo?

seppo.social/webroot/demo/
