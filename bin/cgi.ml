(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * cgi.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let ( let* ) = Result.bind
let ( >>= )  = Result.bind
let ( >>| ) a b = match a with
  | Error _ as e -> Lwt.return e
  | Ok a -> b a

(* still does a Lwt_main.run *)
let handle uuid tnow ic (req : Cgi.Request.t) : Cgi.Response.t =
  let t0 = Sys.time() in
  Logr.debug (fun m -> m "%s.%s %a %s %s %s" "Cgi" "handle" Uuidm.pp uuid req.remote_addr req.request_method req.path_info);
  let redir_if_auth_miss (r : Cgi.Request.t) =
    let loc = Web.Credentials.path in
    if (Auth.fn |> File.exists) || (r.path_info |> String.equal loc)
    then Ok r
    else
      let s302 ?(header = []) p = r.script_name ^ p |> Http.s302 ~header in
      let* sec = Result.map_error
          (Http.err500 "expected cookie secret")
          (Cfg.CookieSecret.(make "" >>= from_file)) in
      (* start a 'recovery' session *)
      let header = [ Web.MyCookie.new_session ~tnow sec req Auth.dummy ] in
      s302 ~header loc
  and restore_assets lst r = let _ = Assets.Const.restore_if_nonex File.pFile lst in
    Ok r in
  let dispatch (r : Cgi.Request.t) =
    (* Logr.debug (fun m -> m "%s.%s path_info '%s'" "Cgi" "handle.dispatch" r.path_info); *)
    let fn_tok = "app/var/run/token" in
    let send_file ct p = p
                         |> File.to_string
                         |> Http.clob_send uuid ct
    and send_res ct p = match p |> Res.read with
      | None   -> Http.s500
      | Some b -> Http.clob_send uuid ct b
    and ases = Web.ases tnow
    and auth = Web.uid_redir
    and ban  = Ban.escalate Ban.cdb
    and csrf_mk v =
      Logr.debug(fun m -> m "%s.%s create and store token" "Cgi" "handle.dispatch");
      (* is it ok to use the request uuid as token? *)
      let tok = uuid |> Uuidm.to_string in
      let mode = [ Open_binary; Open_creat; Open_trunc; Open_wronly ] in
      File.out_channel ~mode fn_tok (fun oc -> output_string oc tok);
      Ok (tok, v)
    and csrf_ck v =
      Logr.debug (fun m -> m "%s.%s load and purge token" "Cgi" "handle.dispatch");
      let tok = File.in_channel fn_tok input_line in
      Web.check_token (fun () -> Unix.unlink fn_tok) tok v
    and form ic r = Ok (Http.Form.of_channel ic, r)
    and base () = (* lazy, may not exist yet *) Cfg.Base.(from_file fn) |> Result.get_ok
    and s302
        ?(qs="")
        ?(header = [])
        p =
      r.script_name ^ p ^ qs
      |> Http.s302 ~header
    and rt = Lwt.return in
    let re = match r.path_info, r.request_method |> Cohttp.Code.method_of_string with
      | ("/var/lock/challenge" as p,   `GET) -> let f = "app" ^ p in f |> send_file Http.Mime.text_plain |> rt
      | ("/doap.rdf" as p,             `GET) -> p |> send_res Http.Mime.text_xml |> rt
      | ("/LICENSE" as p,              `GET)
      | ("/version" as p,              `GET) -> p |> send_res Http.Mime.text_plain |> rt
      | "/search",                     `GET  -> Http.s501 |> rt
      | "/ping",                       `GET  -> r |>                                           Web.Ping.get ~base uuid |> rt
      |"/activitypub/inbox/index.jlda",`POST
      | "/activitypub/inbox/index.jsa",`POST
      | "/activitypub/inbox.jsa",      `POST -> r |>                                           Ap.Inbox.post ~base uuid tnow ic
      (* | "/activitypub/monitor.txt", `GET  -> r |> Activitypub.monitor *)
      | "/credentials",                `GET  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Credentials.get uuid |> rt
      | "/credentials",                `POST -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Web.Credentials.post uuid tnow |> rt
      | "/login",                      `GET  -> r |>                               csrf_mk >>= Web.Login.get uuid |> rt
      | "/login",                      `POST -> r |>                   form ic >>= csrf_ck >>= Web.Login.post uuid tnow ban |> rt
      | "/logout",                     `GET  -> r |> ases                                  >>= Web.Logout.get uuid |> rt
      | "/edit",                       `GET
      | "/post",                       `GET  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Post.get ~base uuid |> rt
      | "/edit",                       `POST
      | "/post",                       `POST -> r |> ases >>= auth >>= form ic >>= csrf_ck >>| Web.Post.post ~base uuid tnow
      | "/actor",                      `GET  -> r |> ases >>= auth >>=             csrf_mk >>| Web.Actor.get uuid
      | "/actor",                      `POST -> r |> ases >>= auth >>= form ic >>= csrf_ck >>| Web.Actor.post ~base uuid tnow
      | "/health/",                    `GET  -> r |> ases >>= auth                         >>= Web.Health.get ~base uuid |> rt
      | "/http",                       `GET  -> r |> ases >>= auth                         >>| Web.Http_.get ~base uuid tnow
      | "/note",                       `GET  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Note.get uuid |> rt
      | "/profile",                    `GET  -> r |> ases >>= auth >>=             csrf_mk >>= Web.Profile.get uuid |> rt
      | "/profile",                    `POST -> r |> ases >>= auth >>= form ic >>= csrf_ck >>= Web.Profile.post uuid tnow |> rt
      | "/session",                    `GET  -> r |> ases                                  >>= Web.Session.get uuid |> rt
      | "/subscribe",                  `GET  -> r |> Result.ok                             >>| Web.Subscribe.get ~base uuid tnow
      | "/tools",                      `GET  -> Http.s501 |> rt
      | "/tools",                      `POST -> Http.s501 |> rt
      | "/",                           `GET  -> ".." |> Http.s302 |> rt
      | "",                            `GET when "" = r.query_string -> Http.s302 "." |> rt
      | "",                            `GET  -> (let q = r.query_string |> Uri.query_of_encoded in
                                                 (* shaarli compatibility *)
                                                 match q |> List.assoc_opt "do" with
                                                 | Some ["login"]     -> s302 Web.Login.path
                                                 | Some ["logout"]    -> s302 Web.Logout.path
                                                 | Some ["configure"] -> s302 Web.Profile.path
                                                 | _                  ->
                                                   (* accessing random urls leads to a ban, eventually *)
                                                   ban tnow r.remote_addr;
                                                   Http.s404
                                                ) |> rt
      | _,                             `GET
        when
          assert ("acct" = Webfinger.Client.scheme);
          r.path_info |> St.starts_with ~prefix:"/@"
          || r.path_info |> St.starts_with ~prefix:"/acct:" ->
        (let prefix,rfc7033 =
           match r.path_info |> St.after ~prefix:"/@" with
           | Some we -> "@", we
           | None -> match r.path_info |> St.after ~prefix:"/acct:" with
             | Some we -> "acct:", we
             | None    -> Logr.err (fun m -> m "%s.%s fatal, cannot happen" "Cgi" "handle.dispatch"); ("","") in
         match prefix ^ rfc7033 |> Webfinger.Client.from_string ~prefix with
         | Error e ->
           Logr.warn (fun m -> m "%s.%s %s" "Cgi" "handle.dispatch" e);
           Error (`Bad_request, [Http.H.ct_plain], fun oc -> e |> output_string oc)
           |> rt
         | Ok rfc7033 ->
           let key = None in (* sign the get request for remote actor profile for calckey? *)
           let u = rfc7033 |> Webfinger.Client.well_known_uri in
           let%lwt fi = u |> Webfinger.Client.http_get_remote_finger ~key in
           (match fi with
            | Error e  ->
              Logr.warn (fun m -> m "%s.%s %s" "Cgi" "handle.dispatch" e);
              Http.s502
            | Ok v     -> match v.links |> As2_vocab.Types.Webfinger.self_link with
              | None   -> Http.s502
              | Some u ->
                let path = r.script_name ^ Web.Actor.path in
                let query = [("id", [u |> Uri.to_string])] in
                Uri.make ~path ~query ()
                |> Uri.to_string
                |> Http.s302)
           |> rt )
      | _                                  ->
        (* accessing random urls leads to a ban, eventually *)
        ban tnow r.remote_addr;
        Http.s404 |> rt in
    re |> Lwt_main.run
  and merge (x : (Cgi.Response.t, Cgi.Response.t) result) : Cgi.Response.t =
    let (status,_,_) as x = match x with
      | Ok x    -> x
      | Error x -> x in
    Logr.info (fun m -> m "%s.%s %a dt=%.3fs HTTP %s %s %s -> localhost%s"
                  "Cgi" "handle"
                  Uuidm.pp uuid
                  (Sys.time() -. t0)
                  (status |> Cohttp.Code.string_of_status)
                  req.request_method
                  req.remote_addr
                  (req |> Cgi.Request.path_and_query));
    x in
  Ok req
  >>= Ban.(check_req (prepare_cdb cdb) tnow)
  >>= restore_assets Assets.Const.all
  >>= redir_if_auth_miss
  >>= dispatch
  |> merge
