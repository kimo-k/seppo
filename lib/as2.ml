(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

(*
 * https://www.w3.org/TR/activitystreams-core/
 * https://www.w3.org/TR/activitystreams-core/#media-type
 *)

module No_p_yes = struct
  type t = No | Pending | Yes
  let to_string = function
    | No      -> "no"
    | Pending -> "pending"
    | Yes     -> "yes"
  let of_string = function
    | "no"      -> Some No
    | "pending" -> Some Pending
    | "yes"
    | "on"      -> Some Yes (* convenience for html form submit feedback *)
    | _         -> None
end

let examine_response j =
  let ok = function
    | `O ["error", `String e] -> Error e
    | _ ->
      Logr.warn (fun m -> m "unknown response: %s" j);
      Error ("unknown response: " ^ j) in
  let error = function
    | `Error _ as e ->
      let e = e |> Ezjsonm.read_error_description in
      Logr.warn (fun m -> m "json parsing error: '%s' in '%s'" e j);
      Error ("json parsing error '" ^ e ^ "' in '" ^ j ^ "'")
    | `Unexpected _ ->
      Logr.warn (fun m -> m "unexpected json: '%s'" j);
      Error ("unexpected json '" ^ j ^ "'")
    | `End_of_input -> Error "end of input"
  in
  j
  |> Ezjsonm.value_from_string_result
  |> Result.fold ~ok ~error
(*
let ns_as  = As2_vocab.Constants.ActivityStreams.ns_as
let ns_sec = As2_vocab.Constants.ActivityStreams.ns_sec
let public = As2_vocab.Constants.ActivityStreams.public
*)
