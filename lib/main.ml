(*
 *    _  _   ____                         _ 
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( >>= )  = Result.bind
let ( let* ) = Result.bind
let ( let+ ) = Result.map

let lwt_err e = Lwt.return (Error e)

let ( ^/ ) a b =
  let p = Uri.path a in
  let p' = p ^ b in
  Uri.with_path a p'

(* may go to where PubKeyPem is: As2 *)
let post_signed
    ?(date = Ptime_clock.now ())
    ?(headers = [ Http.H.ct_jlda; Http.H.acc_app_jlda ])
    ~uuid
    ~key_id
    ~pk
    body
    uri =
  Logr.debug (fun m -> m "%s.%s %a keyid: %a" "Http" "post_signed" Uuidm.pp uuid Uri.pp key_id);
  assert (key_id |> Uri.to_string |> St.ends_with ~suffix:"/actor.jsa#main-key");
  let he_sig = Ap.PubKeyPem.(Http.signed_headers (key_id,sign pk,date) (digest_base64' body) uri) in
  let headers = Cohttp.Header.add_list he_sig headers in
  Http.post ~headers body uri

let send_http_post ?(fkt = Lwt.return) ~uuid ~key_id ~pk (msg_id, uri, body) =
  Logr.debug (fun m -> m "%s.%s %a / %a %a" "Main" "send_http_post" Uri.pp msg_id Uri.pp uri Uuidm.pp uuid);
  let%lwt r = post_signed ~uuid ~pk ~key_id body uri in
  match r with
  | Error _   as e -> e |> Lwt.return
  | Ok (re,_) as o ->
    (match re.status with
     | #Cohttp.Code.success_status -> fkt o
     | _                           -> re.status
                                      |> Cohttp.Code.string_of_status
                                      |> Result.error
                                      |> Lwt.return)

(* https://www.w3.org/TR/activitypub/#delivery *)
let notify_remote_actor ~uuid ~pk ~sndr (msg_id, ibox, _, body) =
  Logr.debug (fun m -> m "%s.%s %a %a %a" "Main" "notify_remote_actor" Uri.pp ibox Uri.pp msg_id Uuidm.pp uuid);
  let key_id = Uri.with_fragment sndr (Some "main-key") in
  assert (sndr |> Uri.to_string |> St.ends_with ~suffix:"/actor.jsa");
  assert (key_id|> Uri.to_string |> St.ends_with ~suffix:"/actor.jsa#main-key");
  post_signed ~uuid ~pk ~key_id body ibox

(* must correspond to dispatch_job *)
let job_encode_notify msg_id (ibox, id) (json : Ezjsonm.value) : Csexp.t =
  let msg_id = msg_id |> Uri.to_string
  and ibox   = ibox   |> Uri.to_string
  and id     = id     |> Uri.to_string
  and json   = json   |> Ezjsonm.value_to_string in
  Csexp.(List [Atom "2"; Atom msg_id; Atom "notify"; List [Atom ibox; Atom id; Atom json]])

let dispatch_job ~base ~pk j payload =
  let sndr = Ap.(base ^/ proj) in
  let key_id = Uri.with_fragment sndr (Some "main-key") in
  let uuid = Uuidm.v `V4 in
  Logr.debug (fun m -> m "%s.%s %s %a" "Main" "dispatch_job" j Uuidm.pp uuid);
  assert (sndr |> Uri.to_string |> St.ends_with ~suffix:"/actor.jsa");
  assert (key_id |> Uri.to_string |> St.ends_with ~suffix:"/actor.jsa#main-key");
  let open Csexp in
  match payload with
  | List [Atom "2"; Atom msg_id; Atom "http.post"; List [Atom uri; Atom body]] ->
    send_http_post ~uuid ~key_id ~pk (msg_id |> Uri.of_string, uri |> Uri.of_string, body)
  | List [Atom "2"; Atom msg_id; Atom "notify"; List [Atom ibox; Atom id; Atom json]] ->
    (* must correspond to job_encode_notify *)
    let%lwt r = notify_remote_actor ~uuid ~sndr ~pk (msg_id |> Uri.of_string, ibox |> Uri.of_string, id |> Uri.of_string, json) in
    let _ = (match r with
        | Ok (rsp,bod) ->
          let%lwt b = bod |> Cohttp_lwt.Body.to_string in
          Logr.debug (fun m -> m "%s.%s %a %s Response: %a\n\n%s" "Main" "dispatch_job" Uuidm.pp uuid ibox Cohttp.Response.pp_hum rsp b);
          Lwt.return (Ok (rsp,bod))
        | _ -> Lwt.return (Error "aua")
      ) in
    (* examine response *)
    Lwt.return r
  |  _ ->
    Logr.err (fun m -> m "%s %s.%s invalid job format %s" E.e1016 "Main" "dispatch_job" j);
    Error "invalid job format" |> Lwt.return

let process_queue
    ?(due  = Ptime_clock.now ())
    ?(wait = Job.wait)
    ?(new_ = Job.new_)
    ?(run  = Job.run)
    ?(cur  = Job.cur)
    ~base
    ~pk
    que =
  let t0 = Sys.time () in
  Logr.debug (fun m -> m "%s.%s" "Main" "process_queue");
  let _ = run
  and _ = cur in
  (* move those due from wait into new *)
  let rec move_due_wait_new ~wait ~new_ ~due  =
    match Job.(peek_opt_any_due ~due ~wait que) with
    | None -> ()
    | Some j ->
      Job.(move que j wait new_);
      move_due_wait_new ~wait ~new_ ~due
  in
  let Queue que' = que in
  (* TODO pull out Lwt_main.run *)
  let rec loop i =
    match Job.peek_opt new_ que with
    | None -> i
    | Some j ->
      (let open Job in
       move que j new_ run;
       let fn = que' ^ run ^ j
       and error s =
         wait_or_err ~wait que run j;
         Logr.info (fun m -> m "job postponed/cancelled: %s reason: %s" j s)
       and ok _p =
         move que j run cur;
         Logr.info (fun m -> m "job done: %s" j)   in
       let ok p =

         dispatch_job ~base ~pk j p
         |> Lwt_main.run
         |> Result.fold ~ok ~error in

       File.in_channel fn Csexp.input
       |> Result.fold ~ok ~error);
      loop (i+1)
  in
  move_due_wait_new ~wait ~new_ ~due;
  let i = loop 0 in
  Logr.info (fun m -> m "%s.%s finished, %i jobs processed in dt=%.3fs." "Main" "process_queue" i (Sys.time() -. t0));
  Ok que

let trigger_queue_and_forget ~base ~loop_pause =
  Logr.info (fun m -> m "%s.%s %i" "Main" "trigger_queue_and_forget" (loop_pause |> Option.value ~default:(-1)));
  let path = "seppo.cgi/ping"
  and query = match loop_pause with
    | Some s -> [("loop",[Printf.sprintf "%is" s])]
    | None   -> []  in
  let uri = Uri.make ~path ~query () in
  let%lwt f = uri
              |> Http.reso ~base
              |> Http.get ~seconds:0.5 (* fire and forget *) in
  let _ = Sys.opaque_identity f in
  Ok (`OK, [Http.H.ct_plain], Cgi.Response.body "ok")
  |> Lwt.return

let queue_run_fn = "app/var/run/queue"

let trigger_queue
    ?(due  = Ptime_clock.now ())
    ?(wait = Job.wait)
    ?(new_ = Job.new_)
    ?(run  = Job.run)
    ?(cur  = Job.cur)
    ?(lock = queue_run_fn)
    ?(loop_pause = None)
    () =
  Logr.debug (fun m -> m "%s.%s %i" "Main" "trigger_queue" (loop_pause |> Option.value ~default:(-1) ));
  (try if
    let max_age = 4. *. 3600. in
    Unix.time() -. (Unix.stat lock).st_mtime > max_age
     then ( Logr.warn (fun m -> m "%s.%s %s looks stale, I unlink it." "Main" "trigger_queue" lock);
            Unix.unlink lock )
   with | Unix.(Unix_error (ENOENT,"stat",_)) -> ());
  try
    let oc = open_out_gen [ Open_binary; Open_creat; Open_excl; Open_wronly ] File.pFile lock in
  (*
    let fu i = Logr.debug (fun m -> m "%s.%s %i" "Main" "sigterm" i) in
    let _ = Sys.signal Sys.sigterm (Sys.Signal_handle fu) in
  *)
    let r =
      try
        let* base = Cfg.Base.(from_file fn) in
        let* pk   = Ap.PubKeyPem.(private_of_pem pk_pem) in
        let _ = process_queue ~due ~wait ~new_ ~run ~cur ~base ~pk Job.qn in
        Ok ()
      with | e ->
        let msg = Printexc.to_string e in
        Logr.err (fun m -> m "%s %s.%s processing failed %s" E.e1017 "Main" "trigger_queue" msg);
        Error "queue processing failed" in
    let _ = Option.bind
        loop_pause
        (fun s ->
           Unix.sleep (max 3 s);
           None) in
    close_out oc;
    Unix.unlink lock;
    let _ = Option.bind
        loop_pause
        (fun _ ->
           Logr.debug (fun m -> m "%s.%s loop" "Main" "trigger_queue");
           let _ = Cfg.Base.(from_file fn)
             >>= (fun base ->
                 assert (loop_pause |> Option.is_some);
                 trigger_queue_and_forget ~base ~loop_pause
                 |> Lwt_main.run
                 |> Result.ok
               ) in
           None) in
    r
  with
  | Sys_error s ->
    Logr.debug (fun m -> m "%s.%s race prevented %s" "Main" "trigger_queue" s);
    Error "race"
  | e ->
    let msg = Printexc.to_string e in
    Logr.err (fun m -> m "%s %s.%s %s" E.e1018 "Main" "trigger_queue" msg);
    Error msg

(* monitor outgoing url and add to <link>? *)
let sift_urls (e : Rfc4287.Entry.t)  =
  Logr.debug (fun m -> m "%s.%s not implemented." "Main" "sift_urls");
  Ok e

(* Extract tags from a post into a list.
 *
 * Needs the post and a tag store. Modifies both.
*)
let sift_tags cdb (e : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "%s.%s" "Main" "sift_tags");
  let open Rfc4287 in
  let c2t init ((Label (Single l),_,_) : Rfc4287.Category.t)  =
    (Tag.Tag ("#" ^ l)) :: init
  in
  let t2c init (Tag.Tag t) =
    Logr.debug (fun m -> m "%s.%s %s" "Main" "sift_tags" t);
    let le = t |> String.length in
    assert (1 < le);
    let t = if '#' == t.[0]
      then String.sub t 1 (le-1)
      else t in
    let t = Single t in
    let l = Category.Label t in
    let te = Category.Term t in
    (l, te, Rfc4287.tagu) :: init
  in
  let ti = e.title in
  let co = e.content in
  let tl = e.categories |> List.fold_left c2t [] in
  let ti,co,tl = Tag.cdb_normalise ti co tl cdb in
  Ok {e with
      title = ti;
      content = co;
      categories = tl |> List.fold_left t2c []}

let find_handles s =
  s
  |> Lexing.from_string
  |> Plain2handle.handle []

(* find mentions *)
let sift_handles (e : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "%s.%s not implemented." "Main" "sift_handles");
  (* Ok ((e.title |> find_handles) @ (e.content |> find_handles)) *)
  Ok e

module Note = struct
  let load_basics () =
    let* base = Cfg.Base.(from_file fn) in
    let* prof = Cfg.Profile.(from_file fn) in
    let* Auth.Uid userinfo,_ = Auth.(from_file fn) in
    let host = base |> Uri.host |> Option.value ~default:"-" in
    let auth = Uri.make ~userinfo ~host () in
    Ok (base,prof,auth)

  module Atom = struct
    let rule = ({
        target        = "%-%/index.xml";
        prerequisites = "app/var/lib/%/%.ix" :: Cfg.Base.fn :: Cfg.Profile.fn :: [];
        fresh         = Make.Outdated;
        command       = (fun _p _rz r t ->
            let* base,prof,auth = load_basics () in
            assert ("%-%/index.xml" |> String.equal r.target);
            assert ("app/var/lib/%/%.ix" |> String.equal (r.prerequisites |> List.hd));
            let src,_,v = t |> Make.src_from r in
            Logr.debug (fun m -> m "%s.%s %s %s -> %s" "Main.Note.Atom" "rule" _p src t);
            let ix = match v with
              | [a;b] -> (a,b |> int_of_string)
              | _ -> failwith __LOC__
            and max     = Storage.Page.( t |> of_fn |> Option.get |> find_max |> to_int )
            and now     = Ptime_clock.now () in
            let author  = auth
            and lang    = prof.language
            and title   = prof.title
            and tz      = prof.timezone
            and self,first,last,prev,next = ix |> Rfc4287.Feed.compute_links ~max ~base:Uri.empty in
            let updated = now |> Rfc3339.of_ptime ~tz in
            let* l = src |> File.in_channel' Csexp.input_many in
            let r = l
                    |> List.fold_left Storage.fold_of_twopad10 []
                    |> Rfc4287.Feed.to_atom_
                      ~base
                      ~self
                      ~prev
                      ~next
                      ~first
                      ~last
                      ~title
                      ~updated
                      ~lang
                      ~author
                      t
                    |> Rfc4287.Feed.to_file t in
            (* HOW to (re-)create the softlink in case *)
            (* let _,_ = mk_unnumbered_syml (depth,unn,p) fn in *)
            r
          );
      } : Make.t)
  end

  let publish ~base ~(profile : Cfg.Profile.t) ~(author : Uri.t) (n : Rfc4287.Entry.t) =
    Logr.debug (fun m -> m "%s.%s '%s'" "Main.Note" "publish" n.title);
    (* determine id and do store app/var/lib/o/p.s *)
    (* add to indices (p,d,t) *)
    (* (2,"o/p",4) app/var/lib/o/p.s app/var/lib/o/p/4.ix -> o/p-4/index.xml *)
    (* (3,"o/d/2023-20-13",4) app/var/lib/o/d/2023-10-13/4.ix -> o/d/2023-10-13-4/index.xml *)
    (* (3,"o/t/tag",4) app/var/lib/o/t/tag/4.ix -> o/t/tag-4/index.xml *)
    (* add to storage and indices (main,date,tags)) *)
    let items_per_page = profile.posts_per_page in
    let n,(a,_b as ix),pos = n |> Storage.save ~items_per_page in
    assert (a |> String.equal "o/p");
    let append_to_page pos init pa = (pos |> Storage.Page.append pa);
      pa :: init in
    let ix_other : Storage.Page.t list = n
                                         |> Storage.Page.next_other_pages ~items_per_page
                                         |> List.fold_left (append_to_page pos) [] in
    (* refresh feeds, outbox etc. *)
    let lang = profile.language in
    let title = profile.title in
    let updated = n.updated (* more precisely would be: now *) in
    let mater init ix = (ix |> Storage.page_to_atom ~base ~title ~updated ~lang ~author) :: init in
    let l = ix :: ix_other
            |> List.fold_left mater [] in
    assert ( 1 + 1 + (n.categories |> List.length) == (l |> List.length));
    Ok n

  let delete ~base ~(profile : Cfg.Profile.t) ~(author : Uri.t) (id : Uri.t) =
    Logr.debug (fun m -> m "%s.%s '%a'" "Main.Note" "delete" Uri.pp id);
    let ru = Atom.rule in
    let j = ru.target |> Make.Jig.make in
    let index_xml (di,pg : Storage.Page.t) : string =
      [di;pg |> string_of_int] |> Make.Jig.paste j |> Option.get in
    let _ = profile
    and _ = base
    and _ = author in
    let* n,ixs = Storage.delete id in
    let _fresh = ixs
                 |> List.fold_left (fun init ix ->
                     (* maybe inject base,profile, etc into Atom.rule to avoid re-loading *)
                     (ix |> index_xml |> Make.make [ ru ])
                     :: init
                   ) [] in
    Ok n
end

(** Enqueue jobs.
 *
 * https://www.w3.org/TR/activitypub/#delivery says "Servers MUST de-duplicate
 * the final recipient list." which implies each actor profile / inbox lookup
 * can lag delivery for all.
 *
 * How long could such a consolidated inbox list be cached? In theory not at
 * all because each inbox target url may change without further notice.
 *
 * In pratice, we will use the inbox as long as it works and redo the
 * webfinger/actor lookup otherwise.
 *
 * 1. get all actor profiles (limit redirects) and extract inbox url
 * 2. de-duplicate
 * 3. deliver to all
 * 4. retry temporary failures
 * 5. handle permanent failures to clean link rot
*)
let notify_subscribers
    ?(due = Ptime_clock.now ())
    ?(que = Job.qn)
    ~base
    followers_fold
    (n : Rfc4287.Entry.t) =
  Logr.debug (fun m -> m "%s.%s" "Main" "notify_subscribers");
  let c = n
          |> Ap.Note.of_rfc4287
          |> Ap.Note.diluviate in
  (* let c = {c with to_ = [id]} in *)
  let c = c |> Ap.Note.mk_create in
  let json = c
             |> As2_vocab.Encode.(create ~base ~context:As2_vocab.Constants.ActivityStreams.und
                                    (note ~base)) in
  let fkt_enqueue init (id, (v : Ap.Followers.State.t' option)) =
    match v with
    | None -> init
    | Some (_,_,ibox,_,_,_) ->
      Logr.debug (fun m -> m "%s.%s %a -> %a" "Main" "notify_subscribers" Uri.pp id Uri.pp ibox);
      let _ = job_encode_notify n.id (ibox, id) json
              |> Csexp.to_string
              |> Bytes.of_string
              |> Job.enqueue ~due que 0 in
      init in
  Logr.debug (fun m -> m "enqueue a job for each follower");
  let _ = followers_fold fkt_enqueue in
  Ok n

let notify_subscribers'
    ?(due = Ptime_clock.now ())
    ?(que = Job.qn)
    ?(cdb = Ap.Followers.cdb)
    ~base =
  let fofo fkt = Mapcdb.fold
      (fun x (k,v) ->
         fkt x (k
                |> Bytes.to_string
                |> Uri.of_string,
                v
                |> Bytes.to_string
                |> Csexp.parse_string
                |> Ap.Followers.State.decode'))
      cdb () in
  notify_subscribers ~due ~que ~base fofo
