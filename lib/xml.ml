let ns_rdf    = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
let ns_xsd    = "http://www.w3.org/2001/XMLSchema#"
(* and ns_sec    = As2_vocab.Constants.ActivityStreams.ns_sec ^ "#" *)
let ns_a      = "http://www.w3.org/2005/Atom"
let ns_seppo  = "http://seppo.social/2023/ns#"
let ns_health = "http://seppo.social/2023/health#"
let ns_rfc7033 = "urn:ietf:rfc:7033"

let to_buf ?(xslt = None) ?(readme = None) ?(indent = None) (x : _ Xmlm.frag) (dst : Buffer.t) =
  let piw n v =
    (* TODO check syntax *)
    Printf.bprintf dst "<?%s %s?>\n" n v in
  piw "xml" "version=\"1.0\"";
  (match xslt with
   | Some v -> piw "xml-stylesheet" (Printf.sprintf "type='text/xsl' href='%s'" v)
   | None       -> ());
  (match readme with
   | Some v -> Printf.bprintf dst "<!--%s-->\n" v
   | None   -> ());
  let o = Xmlm.make_output ~decl:false ~indent (`Buffer dst) in
  Xmlm.output_doc_tree (fun x -> x) o (None,x)

let to_chan ?(xslt = None) ?(readme = None) ?(indent = None) (x : _ Xmlm.frag) dst =
  let piw n v =
    (* TODO check syntax *)
    Printf.fprintf dst "<?%s %s?>\n" n v in
  piw "xml" "version=\"1.0\"";
  (match xslt with
   | Some v -> piw "xml-stylesheet" (Printf.sprintf "type='text/xsl' href='%s'" v)
   | None       -> ());
  (match readme with
   | Some v -> Printf.fprintf dst "<!--%s-->\n" v
   | None   -> ());
  let o = Xmlm.make_output ~decl:false ~indent (`Channel dst) in
  Xmlm.output_doc_tree (fun x -> x) o (None,x)
