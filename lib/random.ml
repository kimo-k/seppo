(* https://raw.githubusercontent.com/aantron/dream/8612f6a661053ba4e0800c9c6c7f39bdefd5f9eb/src/cipher/random.ml *)
(* This file is part of Dream, released under the MIT license. See LICENSE.md
   for details, or visit https://github.com/aantron/dream.

   Copyright 2021 Anton Bachin *)



(* TODO LATER Is there something with lighter dependencies? Although perhaps
   these are not so bad... *)

let random_buffer = Mirage_crypto_rng.generate

let random n =
  Cstruct.to_string (random_buffer n)

