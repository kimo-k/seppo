
let camel = "🐫"

(* has an alloc, but part of ocaml >= 4.13 anyway *)
let starts_with ~prefix s =
  let lp = prefix |> String.length in
  lp <= (s |> String.length)
  && (prefix |> String.equal (String.sub s 0 lp))

let after ~prefix s : string option =
  let lp = prefix |> String.length in
  if lp <= (s |> String.length)
  && (prefix |> String.equal (String.sub s 0 lp))
  then Some (String.sub s lp ((s |> String.length) - lp))
  else None

let before ~suffix s : string option =
  let ls = suffix |> String.length
  and l = s |> String.length in
  if ls <= l
  && (suffix |> String.equal (String.sub s (l - ls) ls))
  then Some (String.sub s 0 (l - ls))
  else None

(* has an alloc, but part of ocaml >= 4.13 anyway *)
let ends_with ~suffix s =
  let ls = suffix |> String.length
  and l = s |> String.length in
  ls <= l
  && (suffix |> String.equal (String.sub s (l - ls) ls))

let updir path =
  match path
        |> String.split_on_char '/'
        |> List.fold_left (fun (l,i) s ->
            match s,i with
            | ("",0)  -> (l,0)
            | (_,0)   -> (l,1)
            | (".",i) -> (l,i)
            | _       -> (".." :: l,i+1)) ([],0) with
  | [],_ -> ""
  | l,_  -> (l |> String.concat "/") ^ "/"

