(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

let ( let* ) = Result.bind

let pre        = "app/var/lib/"
let fn         = pre ^ "o/p.s"
let fn_id_cdb  = Mapcdb.Cdb (pre ^ "o/id.cdb")
let fn_url_cdb = Mapcdb.Cdb (pre ^ "o/url.cdb")
let fn_t_cdb   = Mapcdb.Cdb (pre ^ "o/t.cdb")

module Fifo = struct
  type t        = string * int

  let make size fn =
    (fn,size)

  let push byt (fn,size) =
    let sep = '\n' in
    let len = byt |> Bytes.length in
    let keep = size - len - 1 in
    let mode = [ Open_append; Open_binary; Open_creat; Open_wronly ] in
    if keep < try (Unix.stat fn).st_size with _ -> 0
    then (* make space and add *)
      let ret = len |> Bytes.create in
      let buf = keep |> Bytes.create in
      File.in_channel fn (fun ic ->
          really_input ic ret 0 len;
          let _ = input_char ic in
          really_input ic buf 0 keep );
      File.out_channel ~mode fn (fun oc ->
          output_bytes oc buf;
          output_bytes oc byt;
          output_char oc sep
        );
      Some ret
    else (* just add *)
      (File.out_channel ~tmp:None ~mode fn (fun oc ->
           output_bytes oc byt;
           output_char oc sep
         );
       None)
end

let id_to_b id =
  id |> Uri.to_string |> Bytes.of_string

(* a tuple of two (file) positions *)
module TwoPad10 = struct
  let length = 28
  type t = int * int

  let encode (a,b : t) =
    (* write a canonical s-expression in one go *)
    Printf.sprintf "(10:0x%08x10:0x%08x)" a b

  let decode (sx : Csexp.t) : (t,'a) result =
    let h2i = int_of_string in
    match sx with
    | Csexp.(List [Atom p0; Atom p1]) -> Ok (h2i p0, h2i p1)
    | _                               -> Error "couldn't decode"

  let decode_many l : t list =
    let h2i = int_of_string in
    l |> List.fold_left (fun init e ->
        match e with
        | Csexp.(List [Atom p0; Atom p1]) -> (h2i p0, h2i p1) :: init
        | _                               -> init) []

  let fold_decode a (_ : (Csexp.t,'a) result) =
    a

  let from_channel ic =
    match Csexp.input_many ic with
    | Error _ -> []
    | Ok l -> decode_many l

  let from_file fn =
    File.in_channel fn from_channel

  let from_id ~fn_id_cdb id : (t,string) result =
    let* b = Mapcdb.find_opt (id_to_b id) fn_id_cdb
             |> Option.to_result ~none:"not found" in
    let* b = b
             |> Bytes.to_string
             |> Csexp.parse_string
             |> Result.map_error (fun _ -> "parse") in
    match [b] |> decode_many with
    | [pos] -> Ok pos
    | _     -> Error "TwoPad10.from_id"

  let strut (p0,p1 : t) =
    assert (p1 - p0 - 6 >= 0);
    let l0,l1 = match p1 - p0 - 6 with
      |            0 as n -> 0,n - 0
      |           10 as n -> 1,n - 1
      |          101 as n -> 1,n - 2
      |        1_002 as n -> 1,n - 3
      |       10_003 as n -> 1,n - 4
      |      100_004 as n -> 1,n - 5
      |    1_000_005 as n -> 1,n - 6
      |   10_000_006 as n -> 1,n - 7
      |  100_000_007 as n -> 1,n - 8
      |1_000_000_008 as n -> 1,n - 9
      (*
      | n when n - 0 <          10 -> 0,n - 0
      | n when n - 1 <         100 -> 0,n - 1
      | n when n - 2 <       1_000 -> 0,n - 2
      | n when n - 3 <      10_000 -> 0,n - 3
      | n when n - 4 <     100_000 -> 0,n - 4
      | n when n - 5 <   1_000_000 -> 0,n - 5
      | n when n - 6 <  10_000_000 -> 0,n - 6
      | n when n - 7 < 100_000_000 -> 0,n - 7
    *)  | n ->
        let n'   = n |> float_of_int in
        let dec' = n' |> log10 |> floor in
        let dec  = n' -. dec' |> log10 |> int_of_float in
        0,n - dec
    in
    let fil = 'x' in
    let r = Csexp.(List [Atom (String.make l0 fil); Atom (String.make l1 fil)]) in
    Logr.debug (fun m -> m "%s.%s %d" "storage" "strut" (p1-p0));
    assert ((p1-p0) == (r |> Csexp.to_string |> String.length));
    r

  let overwrite fn (p0,p1 as pos : t) =
    File.out_channel
      ~tmp:None
      ~mode:[ Open_binary; Open_wronly ]
      fn
      (fun oc ->
         seek_out oc p0;
         assert (p0 == pos_out oc);
         pos |> strut |> Csexp.to_channel oc;
         assert (p1 == pos_out oc) )
end

let of_twopad10 ?(fn = fn) (p0,_ : TwoPad10.t) : (Csexp.t,'a) result =
  let ipt ic =
    seek_in ic p0;
    Csexp.input ic in
  fn |> File.in_channel' ipt

let fold_of_twopad10 a p =
  let ( >>= ) = Result.bind in
  match p
        |> TwoPad10.decode
    >>= of_twopad10
    >>= Rfc4287.Entry.decode with
  | p -> p :: a

module Page = struct
  type t = string * int

  let jig = pre ^ "%/%.ix" |> Make.Jig.make

  let of_fn fn : t option =
    match fn |> Make.Jig.cut jig with
    | Some [a;b] -> Some (a,b |> int_of_string)
    | _          -> None

  let to_fn (a,b : t ) =
    [a;b |> string_of_int] |> Make.Jig.paste jig |> Option.get

  let find_max ?(pre = pre) (dir,_ : t) =
    assert (not (dir |> St.ends_with ~suffix:"/"));
    let mx = File.fold_dir (fun c fn ->
        (try Scanf.sscanf fn "%d.ix" (fun i -> i)
         with _ -> -1)
        |> max c,true)
        (-1) (pre ^ dir) in
    if mx < 0
    then None
    else Some (dir,mx)

  let modify_idx fu (a,x : t) : t =
    (a,x |> fu)

  let pred = modify_idx Int.pred
  let succ = modify_idx Int.succ

  let to_int = function
    | Some (_,x : t) -> x
    | _ -> -1

  (* the next id and page *)
  let next_id ~items_per_page (dir,_ as pa : t) : (Uri.t * t) =
    (* Logr.debug (fun m -> m "%s.%s %s" "Storage" "next_id" dir); *)
    assert (dir |> St.starts_with ~prefix:"o/");
    assert (not (dir |> St.ends_with ~suffix:"/"));
    let bytes_per_item = TwoPad10.length in
    (* get the previously highest index number and name *)
    let _ = pa |> to_fn |> Filename.dirname |> File.mkdir_p File.pDir in
    let pg,i =
      match pa |> find_max with
      | None ->
        (* Logr.debug (fun m -> m "%s.%s first %s" "Storage" "next_id" dir); *)
        0,0
      | Some (di,pg) ->
        assert (di |> String.equal dir);
        let pa = (dir,pg) in
        let i = (try (pa |> to_fn |> Unix.stat).st_size
                 with _ -> 0) / bytes_per_item in
        if i < items_per_page
        then pg,i
        else pg+1,0
    in
    assert (pg >= 0);
    assert (i >= 0);
    assert (i < items_per_page);
    let j = "%-%/#%" |> Make.Jig.make in
    let v = [dir;pg |> string_of_int;i |> string_of_int] in
    let id = v |> Make.Jig.paste j |> Option.get |> Uri.of_string in
    Logr.debug (fun m -> m "%s.%s %a" "Storage" "next_id" Uri.pp id);
    assert (id |> Uri.to_string |> St.starts_with ~prefix:"o/");
    id,(dir,pg)

  let append (_,b as pa : t) (pos : bytes) =
    assert (b >= 0);
    pa |> to_fn |> File.out_channel' ~tmp:None (fun oc -> output_bytes oc pos)

  let _remake fn ix =
    (* add csexp entry to .s and return (id,position) tuple *)
    let add_1_csx oc sx =
      let ol = pos_out oc in
      sx |> Csexp.to_channel oc;
      let ne = pos_out oc in
      let id = match sx |> Rfc4287.Entry.decode with
        | Error _ -> None
        | Ok r    -> Some r.id in
      (id,(ol,ne)) in
    (* if Some id call fkt with id->(ol,ne) *)
    let add_1_p fkt = function
      | (None,_v)    -> Logr.warn (fun m -> m "add a strut?")
      | (Some id,v)  -> fkt (id_to_b id, v |> TwoPad10.encode |> Bytes.of_string) in
    (* - read all csexps from the source *)
    let ic = open_in_gen [ Open_binary; Open_rdonly ] 0 fn in
    let* sxs = Csexp.input_many ic in
    close_in ic;
    (* copy fn content as csexps to tmp file fn' *)
    let fn' = fn ^ "~" in
    let oc = open_out_gen [ Open_binary; Open_wronly ] File.pFile fn' in
    let cp_csx oc sxs sx = (add_1_csx oc sx) :: sxs in
    let pos = List.fold_left (cp_csx oc) [] sxs in
    close_out oc;
    (* recreate cdb *)
    let none _ = false in
    let add_all fkt = List.iter (add_1_p fkt) pos in
    let _ = Mapcdb.add_many none add_all ix in
    (* swap tmp for real *)
    Unix.rename fn' fn;
    Ok fn

  open Rfc4287

  (* all but o/p/, unnumbered *)
  let other_feeds (e : Entry.t) : t list =
    let day (Rfc3339.T iso) = ("o/d/" ^ String.sub iso 0 10,-3) in
    let open Category in
    let tag init (_,(Term (Single t)),_) = ("o/t/" ^ t,-3) :: init in
    day e.published
    :: (e.categories |> List.fold_left tag [])

  (* all but o/p/, numbered *)
  let next_other_pages ~items_per_page (e : Entry.t) : t list  =
    let page init item =
      let _,pg = next_id ~items_per_page item in
      pg :: init
    in
    e
    |> other_feeds
    |> List.fold_left page []
end

open Rfc4287

(* all logical feed urls, xml+json, (including the main feed) outbox etc. *)
let feed_urls (e : Entry.t) =
  let db = Uri.make ~path:"o/d/" () in
  let day (Rfc3339.T iso) =
    let p = String.sub iso 0 10 in
    Uri.make ~path:(p ^ "/") () |> Http.reso ~base:db in

  let tb = tagu in
  let open Category in
  let tag (_,(Term (Single p)),_) =
    Uri.make ~path:(p ^ "/") () |> Http.reso ~base:tb in

  let obox = Uri.make ~path:(Ap.apub ^ "outbox/") () in
  defa
  :: obox
  :: (e.published |> day)
  :: (e.categories |> List.map tag)

let climb a : string =
  a
  |> String.split_on_char '/'
  |> List.map (fun _ -> "../")
  |> String.concat ""

let make_feed_syml (unn,b : Page.t) fn' =
  Logr.debug (fun m -> m "%s.%s %s/%d %s" "Storage" "make_feed_syml" unn b fn');
  let ld = unn ^ "/" in
  let ln = ld ^ (Filename.basename fn') in
  let fn = (unn |> climb) ^ fn' in
  Logr.debug (fun m -> m "ln -s %s %s" fn ln);
  let open Unix in
  ((* should we take measures to only ever unlink symlinks? *)
    try unlink ln
    with Unix_error(ENOENT, "unlink", _) -> ());
  (try mkdir ld File.pDir
   with Unix_error(EEXIST, "mkdir", _) -> ());
  symlink ~to_dir:false fn ln;
  (fn, ln)

(* rebuild a single atom page plus evtl. the softlink *)
let page_to_atom ~base ~title ~updated ~lang ~(author : Uri.t) (a,b as ix) =
  Logr.debug (fun m -> m "%s.%s %s/%d" "Storage" "page_to_atom" a b);
  (* fold ix range into entry. *)
  let hydrate sc init (p0,_) =
    let* init = init
                |> Result.map_error
                  (fun e -> Logr.err (fun m -> m "%s.%s ignored %s" "Storage" "page_to_atom.hydrate.a" e);
                    e) in
    seek_in sc p0;
    let* item = Csexp.input sc
                |> Result.map_error
                  (fun e -> Logr.err (fun m -> m "%s.%s ignored %s" "Storage" "page_to_atom.hydrate.b" e);
                    e) in
    match Entry.decode item with
    | Ok item         ->
      Logr.debug (fun m -> m "%s.%s 0x%x %a" "Storage" "page_to_atom.hydrate.0" p0 Uri.pp item.id);
      Ok (item :: init)
    | Error "deleted" ->
      Logr.warn (fun m -> m "%s.%s found a stale index entry 0x%x" "Storage" "page_to_atom.hydrate.1" p0);
      Ok init
    | Error e         ->
      Logr.err (fun m -> m "%s.%s 0x%x ignoring: %s" "Storage" "page_to_atom.hydrate.2" p0 e);
      Ok init in
  let* es = File.in_channel fn (fun sc ->
      ix |> Page.to_fn |> File.in_channel' (fun ic ->
          match Csexp.input_many ic with
          | Error e' as e ->
            Logr.err (fun m -> m "%s.%s %s/%d: %s" "Storage" "page_to_atom" a b e');
            e
          | Ok l -> l
                    |> TwoPad10.decode_many
                    |> List.fold_left (hydrate sc) (Ok [])
        )) |> Result.map_error (fun e ->
      Logr.err (fun m -> m "%s.%s ignored %s" "Storage" "page_to_atom" e);
      e) in
  let self,first,last,prev,next = ix |> Rfc4287.Feed.compute_links ~max:7000 ~base:Uri.empty in
  (* atom index.xml *)
  let j_xml = "%-%/index.xml" |> Make.Jig.make in
  let fn = [a;b|> string_of_int] |> Make.Jig.paste j_xml |> Option.get in
  Logr.debug (fun m -> m "%s.%s %s/%d -> %s (%d entries)" "Storage" "page_to_atom" a b fn (es |> List.length));
  let x = es |> Feed.to_atom
            ~base
            ~self
            ~prev
            ~next
            ~first
            ~last
            ~title
            ~updated
            ~lang
            ~author in
  let _ = fn |> Filename.dirname |> File.mkdir_p File.pDir in
  fn |> File.out_channel' (x |> Xml.to_chan ~xslt:(xslt "posts.xslt" fn));
  let _,_ = make_feed_syml ix fn in
  Ok fn

(* return a list of Page.t the entry is part of *)
let save
    ?(items_per_page = 50)
    ?(fn = fn)
    ?(fn_id_cdb = fn_id_cdb)
    ?(_fn_url_cdb = fn_url_cdb)
    ?(_fn_t_cdb = fn_t_cdb)
    (e : Rfc4287.Entry.t) =
  let rel_edit_for_id id : Rfc4287.Link.t =
    Logr.debug (fun m -> m "%s.%s id %a" "Storage" "save.rel_edit_for_id" Uri.pp id);
    let path = "seppo.cgi/edit" in
    let f = id |> Uri.fragment |> Option.value ~default:"" in
    assert (f != "");
    let query = [("id",[id |> Uri.to_string])] in
    {href    = Uri.make ~path ~query ();
     rel     = Some Link.edit;
     icon    = None;
     rfc7033 = None;
     title   = None} in
  let id,(a,b as ix) = Page.next_id ~items_per_page ("o/p",-3) in
  Logr.debug (fun m -> m "%s.%s  id: %a  fn_x: %s%d" "Storage" "save" Uri.pp id a b);
  assert (Rfc4287.defa |> Uri.to_string |> String.equal (a ^"/"));
  assert (id |> Uri.to_string |> St.starts_with ~prefix:"o/p-");
  assert (a |> String.equal "o/p");
  assert (b >= 0);
  let e = {e with id;
                  links = (id |> rel_edit_for_id) :: e.links} in
  (* append entry to global storage .s and record store position *)
  let p0 = try (Unix.stat fn).st_size with _ -> 0 in
  let mode = [ Open_append; Open_binary; Open_creat; Open_wronly ] in
  File.out_channel ~tmp:None ~mode fn (fun oc ->
      e
      |> Rfc4287.Entry.encode
      |> Csexp.to_channel oc );
  let p1 = (Unix.stat fn).st_size in
  let pos = (p0,p1)
            |> TwoPad10.encode
            |> Bytes.of_string in
  pos |> Page.append ix;
  let _ = Mapcdb.add (id_to_b e.id) pos fn_id_cdb in
  Logr.warn (fun m -> m "@TODO append url->id to urls.cdb");
  e,ix,pos

let from_channel (p0,_ : TwoPad10.t) sc =
  seek_in sc p0;
  sc |> Csexp.input >>= Entry.decode

(* delete from primary storage and ix files. *)
let delete
    ?(fn = fn)
    ?(fn_id_cdb = fn_id_cdb)
    id : (Rfc4287.Entry.t * Page.t list, string) result =
  Logr.debug (fun m -> m "%s.%s %a" "Storage" "delete" Uri.pp_hum id);
  let* pos = id |> TwoPad10.from_id ~fn_id_cdb in
  let* r = fn |> File.in_channel' (from_channel pos) in
  let fkt = TwoPad10.overwrite fn pos;
    (fun init (_a,_b as ix : Page.t) ->
       Logr.info (fun m -> m "%s.%s purge %s-%d from %s" "Storage" "delete" _a _b "ix");
       Logr.debug (fun m -> m "TODO %s.%s %s-%d scanning for pos" "Storage" "delete" _a _b);
       Logr.debug (fun m -> m "TODO %s.%s %s-%d overwrite pos" "Storage" "delete" _a _b);
       ix :: init) in
  let ixs = (Rfc4287.defa |> Uri.to_string,0 : Page.t)
            :: (r |> Page.other_feeds)
            |> List.fold_left fkt [] in
  (*
  - how can we find the ix pos entries to overwrite?
  - do we have to traverse the ix files back to forth to find the pos?
  *)
  (* affected indexes are
     - main feed o/p
     - date yyyy-mm-dd/
     - tags
     - *)
  Logr.warn (fun m -> m "TODO %s.%s %s" "Storage" "delete" "find the feeds containing the id and mark them for refresh");
  Ok (r,ixs)

let select ?(fn = fn) id : (Rfc4287.Entry.t, string) result =
  Logr.warn (fun m -> m "%s.%s %a" "Storage" "select" Uri.pp_hum id);
  let* pos = TwoPad10.from_id ~fn_id_cdb id in
  File.in_channel fn (from_channel pos)
