(*
 *    _  _   ____                         _  
 *  _| || |_/ ___|  ___ _ __  _ __   ___ | | 
 * |_  ..  _\___ \ / _ \ '_ \| '_ \ / _ \| | 
 * |_      _|___) |  __/ |_) | |_) | (_) |_| 
 *   |_||_| |____/ \___| .__/| .__/ \___/(_) 
 *                     |_|   |_|             
 *
 * Personal Social Web.
 *
 * cgi.ml
 *
 * Copyright (C) The #Seppo contributors. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Seppo_lib

let ( let* ) = Result.bind

let index _uuid =
  Ok (`OK, [Http.H.ct_html], fun oc ->
      {|<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="generator" content="Seppo.Social" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Check WebFinger RFC7033 etc.</title>
</head>
<body>
  <p>This checker is a single-file <a href="https://www.ietf.org/rfc/rfc3875.html">cgi</a>. Host
  it yourself and have all logs on your own property. <a href="https://seppo.social/downloads/">Download apchk.cgi</a>, drop it on a
  cgi-capable webserver and off you go without any further ado. The <a href=
  "https://seppo.social/development/">source</a> is free and open.</p>
  <p>It still <a href="https://codeberg.org/seppo/seppo/issues?labels=109423">lacks a lot</a>.</p>
  <h2 id="rfc7033">WebFinger RFC7033</h2>
  <p>Check <a href="https://www.rfc-editor.org/rfc/rfc7033">WebFinger</a> result as required by
  <a href="https://seppo.social/">seppo.social</a>.</p>
  <p>Fetch the document, parse, re-encode and return as json. Usually a subset of the input.</p>
  <form name="webfinger" action="apchk.cgi/webfinger" method="get" enctype=
  "application/x-www-form-urlencoded" id="webfinger">
    <input type="checkbox" name="redirect" id="redirect" checked="checked"/> <label for="redirect">Redirect to AP actor profile check in case</label><br/>
    <input placeholder="alice@example.com" name="acct" type="text" pattern="^[^@]+@[^@]+$" size="45" minlength="5"
    autofocus="autofocus" required="required" /> <input type="submit" />
  </form>
  <h2 id="ap_profile">AP actor profile</h2>
  <p>Check <a href="https://www.w3.org/TR/activitypub/#actor-objects">Activitypub Actor Profile</a>
  result as required by <a href="https://seppo.social/">seppo.social</a>.</p>
  <p>Fetch the document, parse, re-encode and return as json. Usually a subset of the input.</p>
  <form name="actor" action="apchk.cgi/actor" method="get" enctype=
  "application/x-www-form-urlencoded" id="actor">
    <input placeholder="https://example.com/actor/alice" name="id" type="url" size="45"
    minlength="5" required="required" /> <input type="submit" />
  </form>
</body>
</html>|} |> Printf.fprintf oc "%s")

let webfinger _uuid qs =
  match qs |> List.assoc_opt "acct" with
  | Some [acct] ->
    (match acct
           |> Webfinger.Client.from_string
           |> Shell.webfinger with
    | Error e ->
      Logr.debug (fun m -> m "%s.%s %s" "cgi" "webfinger" e);
      Ok (`Bad_request, [Http.H.ct_plain], fun oc -> e |> output_string oc)
    | Ok q ->
      match q.links |> As2_vocab.Types.Webfinger.self_link,
            qs |> List.assoc_opt "redirect" with
      | Some u,Some ["on"] ->
        let r = Uri.make
            ~path:"actor"
            ~query:["id",[u |> Uri.to_string]]
            ()  in
        r
        |> Uri.to_string
        |> Http.s302
      | _,_ ->
        Ok (`OK, [Http.H.ct_json], fun oc ->
            q
            |> As2_vocab.Encode.Webfinger.query_result ~base:Uri.empty
            |> Ezjsonm.value_to_channel oc ))
  | _ -> Http.s400

let actor _uuid qs =
  match qs |> List.assoc_opt "id" with
  | Some [id] ->
    (match id |> Uri.of_string |> Shell.actor with
     | Error e ->
       Logr.debug (fun m -> m "%s.%s %s" "cgi" "actor" e);
       Ok (`Bad_request, [Http.H.ct_plain], fun oc -> e |> output_string oc)
     | Ok q ->
       Ok (`OK, [Http.H.ct_jlda], fun oc ->
           let context = As2_vocab.Constants.ActivityStreams.und in
           q
           |> As2_vocab.Encode.person ~context ~base:Uri.empty
           |> Ezjsonm.value_to_channel oc ))
  | _ -> Http.s400

let handle uuid _ic (req : Cgi.Request.t) : Cgi.Response.t =
  let dispatch (r : Cgi.Request.t) =
    let send_res ct p = match p |> Res.read with
      | None   -> Http.s500
      | Some b -> Http.clob_send uuid ct b in
    match r.path_info, r.request_method |> Cohttp.Code.method_of_string with
    | ("/doap.rdf" as p,          `GET) -> p |> send_res Http.Mime.text_xml
    | ("/LICENSE" as p,           `GET)
    | ("/version" as p,           `GET) -> p |> send_res Http.Mime.text_plain
    | "",                         `GET  -> index uuid
    | "/",                        `GET  -> Http.s302 req.script_name
    | "/actor",                   `GET  -> r.query_string |> Uri.query_of_encoded |> actor uuid
    | "/webfinger",               `GET  -> r.query_string |> Uri.query_of_encoded |> webfinger uuid
    | _,                          `GET  -> Http.s404
    | _                                  -> Http.s405
  and merge = function
    | Ok v    -> v
    | Error v -> v
  in
  Logr.info (fun m -> m "%s -> %s %s" req.remote_addr req.request_method (req |> Cgi.Request.path_and_query));
  req
  |> dispatch
  |> merge
