
# v$ver, $date

Binary:   seppo.cgi
Source:   source.tar.gz
Download: https://Seppo.Social/Linux-x86_64-$ver
Git:      https://Seppo.Social/v/$GIT_SHA
Install:  https://seppo.social/en/support/#installation

Changes

- html formatted notes
- follower list
- deny follower
- publicKey.owner property optional
- html/css/js tweaks
- app/var/run/cookie.sec persistence format
- app/var/lib/notify.cdb persistence format
- various commandline helpers (seppo.cgi -h)
- https optional https://seppo.social/issues/8

Verify the signature with openssl:

$ curl -LO https://Seppo.Social/seppo.pub.pem
$ openssl dgst -verify seppo.pub.pem -keyform PEM -sha256 -signature seppo.cgi.signature -binary seppo.cgi
